#!/usr/bin/python3

# Copyright [2020 - 2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
import datetime as dt
import os
import pathlib
import subprocess
import atexit
import time
import signal
import shlex
import argparse
import sys
import importlib.util
import select
import json
import logging
import csv

from logger import *
from device_types import *
from zbcli_features import *

# Escape commands
SCRIPT_ESCAPE_TSHARK_MARK_COMMAND = "#tshark-mark:"
SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND = "#tshark-search:"
SCRIPT_ESCAPE_TSHARK_SAVE_COMMAND = "#tshark-save:"
SCRIPT_ESCAPE_DEVICE_COMMAND = "#device:"
SCRIPT_ESCAPE_HW_COMMAND = "#hw:"
SCRIPT_ESCAPE_SCRIPT_COMMAND = "#script:"
SCRIPT_ESCAPE_SOURCE_COMMAND = "#source:"
SCRIPT_ESCAPE_INIT_COND_COMMAND = "#init-cond:"
SCRIPT_ESCAPE_TEST_PROC_COMMAND = "#test-proc:"
SCRIPT_ESCAPE_EXP_OUT_COMMAND = "#exp-out:"
SCRIPT_ESCAPE_USER_COMMAND = "#user:"
SCRIPT_ESCAPE_DEFAULT_REPLACE_COMMAND = "#vfab_default_replace:"
SCRIPT_ESCAPE_PICS_DUT_DEVICES = "#pics-role-dut:"
SCRIPT_ESCAPE_COMMANDS = []
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_TSHARK_SAVE_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_TSHARK_MARK_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_DEVICE_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_HW_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_SCRIPT_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_SOURCE_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_INIT_COND_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_TEST_PROC_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_EXP_OUT_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_USER_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_DEFAULT_REPLACE_COMMAND)
SCRIPT_ESCAPE_COMMANDS.append(SCRIPT_ESCAPE_PICS_DUT_DEVICES)
TSHARK_SEARCH_COMMANDS = []
TSHARK_SEARCH_COMMANDS.append(SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND)

# Hardware commands
HW_COMMAND_RESET = "reset"

# Script commands
SCRIPT_COMMAND_ADD_DEVICE = "add-device"
SCRIPT_COMMAND_ADD_KEY = "add-key"
SCRIPT_COMMAND_VAR_MODIFY = "var-mod"
SCRIPT_COMMAND_VAR_NEW = "var-new"
SCRIPT_COMMAND_VAR_MASK_NEW = "var-mask-new"
SCRIPT_COMMAND_VAR_MASK_ADD_CHANNEL = "var-mask-add-channel"
SCRIPT_COMMAND_VAR_MASK_ADD_PAGE = "var-mask-add-page"
SCRIPT_COMMAND_PAUSE = "pause"

# Command List enumerations
COMMAND_LIST_LNO = 0
COMMAND_LIST_CMD = 1

# Script variable enumerations
VARIABLE_TYPE = 0
VARIABLE_DEVICE = 1

# Parsed argument values
COND_ZBCLI_FEATURES = False
COND_PARSE_ZBCLI_SCRIPT_CHANNELS = False
COND_NO_CAPTURE = False
COND_NO_CHECKS = False
COND_NO_PAUSE = False
COND_FAST = False
COND_DISPLAY_LICENSE = False
COND_ADD_INFO = False
COND_USE_SCRIPT_LIST = False
COND_CONVERT_R22_GU_CMDS = False
EXPECTED_DEV_ANNCE_CMDS = []
SNIFFER_CHANNEL_OVERRIDE = []
DEVICES_CONFIG_OVERRIDE = ""
SCRIPTS_PATH = ""
CONFIG_FILE_PATH = ""
DISABLED_TYPES = []
LOG_LABEL = ""
ZDD_EUI48_OVERRIDE = ""
DEVICES_MODULE_PATH = ""
SNIFFERS_MODULE_PATH = ""
LOG_PARSER_FILEPATH = ""
SOURCE_CBKE_CERT_PATH = ""
CHANNEL_CONFIG_PATH = ""
PICS_STRING = ""
CBKE_CERT_LIST = []
DEVICES_MODULE = None
SNIFFERS_MODULE = None
LOG_PARSER_MODULE = None

# Parsed device log values
PARSED_ADDRESSING = 0
PARSED_ZIGBEE_KEY = 1
PARSED_ZIGBEE_DIRECT_KEY = 2
PARSED_TCSO_BACKUP = 3

# Directories
OUTPUT_DIR = "out/" + str(dt.date.today()) + "_" + str(time.strftime("%H%M%S") + "/")
SCRIPT_RESULTS_DIR = ""
SOURCE_CMD_DIR = "./"

# Global lists and variables
TEST_CASE_START_TIME = None
RETRY_COUNT = 0
MORE_DEVICE_LOGS = False
TIMEOUT_COMMAND_DEFAULT = 20
TIMEOUT_OVERRIDE = {}
UNSUPPORTED_GU_CMDS = []
UNSUPPORTED_DUT_CMDS = []
ZIGBEE_KEYS = []
ZIGBEE_KEYS_DEFAULT = []
ZD_KEYS_DEFAULT = []
ZIGBEE_KEYS_SCRIPT = []
INITIALIZED_DEVICES = []
ACTIVE_DEVICE = ""
SCRIPT_FAIL = False
NWKADDR_THREAD = None
USER_REPLACEMENTS_CMD_LINE = {}
USER_REPLACEMENTS = {}
DYNAMIC_REPLACEMENTS = {}
DUT_PREFIXES = ("dut", "d", "DUT")
GU_PREFIXES = ("gu", "g", "GU", "TH", "th")
CUSTOM_INTERFACE_LIST = []
DEV_ANNCE_EVENT = threading.Event()
EXPECTED_DEV_ANNCE = None
TCSO_BACKUP_PARSING_IN_PROGRESS = {}
DUT_OVERRIDE = ""
R22_GU_UTIL = R22_GU_Utility()
CHANNEL_DICT = None

CURRENT_SCRIPT = ""
SCRIPT_LIST = {}
SCRIPT_DICT_CMDS = "cmds"
SCRIPT_DICT_PATH = "path"
SCRIPT_DICT_CAUSE_OF_FAIL = "fail_cmd"
SCRIPT_DICT_PASS = "pass"
SCRIPT_DICT_REPLACEMENTS = "replacements"

# Device
SNIFFER_THREADS = []
ZB_INTERFACE_LIST = []
SNIFFER_CFGS = []
DEVICE_CFGS = []
SCRIPT_DEVICES = {}

# Tshark
TSHARK_MAIN = None
TSHARK_FRAMENUM = 0
TSHARK_FRAMENUM_BACKUP = 0
TSHARK_SEARCH_FAST_TEXT = False
TSHARK_SEARCH_NUM = 0
TSHARK_SEARCH_TIMEOUT = 1
TSHARK_SEARCH_FILTER = 2
TSHARK_SEARCH_FIELD = 3
TSHARK_SEARCH_VARIABLE = 4

# Logging
DEV_LOG = None
LOG_ACTIVE = False
LOG_MUTEX = threading.Lock()
LOG_TEMP_LIST = []

# Zigbee Direct
ZD_KEYS = []
ZDD_CTRL_PAYLOAD = []
BLE_SNIFFER_CFGS = []
BLE_INTERFACE_LIST = []
TEMPDIR = ""
NRF_SNIFFER = None
BLESNIFF_CTRL_OUT = None
BLESNIFF_CTRL_IN = None
BLE_SNIFFER_THREADS = []

@atexit.register
def cleanup():
    global LOG_ACTIVE

    R22_GU_UTIL.reset()
    destroy_radios()
    if not COND_NO_CAPTURE and TSHARK_MAIN:
        for blesniffer in BLE_SNIFFER_THREADS:
            blesniffer.close()
            blesniffer.join()
        for sniffer in SNIFFER_THREADS:
            if sniffer:
                sniffer.close()
                sniffer.join()
    LOG_ACTIVE = False

def add_nwkaddr(data):
    global EXPECTED_DEV_ANNCE

    address_strings = data.strip().split(", ")
    if len(address_strings) == 2:
        extaddr = address_strings[0].lower()
        nwkaddr = address_strings[1].lower()
        if EXPECTED_DEV_ANNCE:
            logging.info_highlight("New addressing from joining device %s (via device logs): %s, %s (extaddr, nwkaddr)" % (EXPECTED_DEV_ANNCE, extaddr, nwkaddr))
            SCRIPT_DEVICES[EXPECTED_DEV_ANNCE]["EXTADDR"] = extaddr
            SCRIPT_DEVICES[EXPECTED_DEV_ANNCE]["NWKADDR"] = nwkaddr
            EXPECTED_DEV_ANNCE = None
            DEV_ANNCE_EVENT.set()
        else:
            for device in SCRIPT_DEVICES:
                if device in INITIALIZED_DEVICES:
                    if SCRIPT_DEVICES[device]["EXTADDR"] == extaddr and SCRIPT_DEVICES[device]["NWKADDR"] != nwkaddr:
                        logging.info_highlight("New network address (via device logs): " + device + "=" + nwkaddr)
                        SCRIPT_DEVICES[device]["NWKADDR"] = nwkaddr

def add_zb_key(data):
    if len(data) != 32:
        logging.warning("Zigbee key has incorrect length or format: '%s'" % data)
        return False

    if data not in ZIGBEE_KEYS and data != "00000000000000000000000000000000":
        logging.info_highlight("Zigbee Key added (via device logs): " + data)
        ZIGBEE_KEYS.append(data)
    return True

def add_zd_key(data):
    zd_strings = data.split(",")
    zd_strings = [entry.strip() for entry in zd_strings]
    if len(zd_strings) != 3 or \
        len(zd_strings[0]) != 16 or \
        len(zd_strings[1]) != 16 or \
        len(zd_strings[2]) != 32:
        logging.warning("Zigbee Direct key has incorrect length or format: '%s'" % data)
        return False

    if (zd_strings not in ZD_KEYS) and (zd_strings[2] != "00000000000000000000000000000000"):
        ZD_KEYS.append(zd_strings)
        logging.info_highlight("Zigbee Direct Key added (via device logs): " + data)
    return True

def add_tcso_backup(data):
    global TCSO_BACKUP_PARSING_IN_PROGRESS

    if "keyPair.deviceAddress:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS.clear()
        logging.info_highlight("Parsing Key information for TCSO of '%s'" % data.split(":")[0])
        TCSO_BACKUP_PARSING_IN_PROGRESS["extaddr"] = "0x" + (data.split(": ")[-1])
    if "keyPair.linkKey:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["linkkey"] = (data.split(": ")[-1])
    if "keyPair.joinAuthMethod:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["initialJoinAuthMethod"] = (data.split(": ")[-1])
    if "keyPair.keyUpdateMethod:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["postJoinKeyUpdateMethod"] = (data.split(": ")[-1])
    if "keyPair.selKeyNegoMethod:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["selKeyNegoMethod"] = (data.split(": ")[-1])
    if "keyPair.preSharedKeyType:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["preSharedKeyType"] = (data.split(": ")[-1])
    if "keyPair.passphraseUpdateAllowed:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["passphraseUpdateAllowed"] = (data.split(": ")[-1])
    if "keyPair.verifiedFrameCounter:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["verifiedFrameCounter"] = (data.split(": ")[-1])
    if "keyPair.passphrase:" in data:
        TCSO_BACKUP_PARSING_IN_PROGRESS["passphrase"] = (data.split(": ")[-1])

    # Need at least deviceAddress and linkKey to be saved for key restore
    if (("Saving link keys to file" in data) or \
        ("Ignoring --filename argument" in data)) and \
        len(TCSO_BACKUP_PARSING_IN_PROGRESS) >= 2:
        device = data.split(":")[0]
        logging.info_highlight("Saved Key Information for TCSO of '%s'\n" % (device))
        SCRIPT_DEVICES[device]["TCSO_BACKUP"].append(TCSO_BACKUP_PARSING_IN_PROGRESS.copy())
        TCSO_BACKUP_PARSING_IN_PROGRESS.clear()

def _test_log_write(msg):
    global DEV_LOG

    for line in msg.splitlines():
        line = line.strip()

        # Print to screen or log file
        if DEV_LOG:
            # Prepend the TH timestamp to the log output
            time_diff = str(get_tc_time_elapsed())
            log_msg = "[%s] %s" % (time_diff, line)
            if DEV_LOG:
                DEV_LOG.write("%s\n" % (log_msg))
            if MORE_DEVICE_LOGS:
                logging.debug(log_msg)

    # Check for keys in the log output, but only if we have a sniffer
    if not COND_NO_CAPTURE and TSHARK_MAIN and LOG_PARSER_MODULE:
        returned = LOG_PARSER_MODULE.parse_keys_and_addresses(msg)
        for item in returned:
            if item[0] == PARSED_ADDRESSING:
                add_nwkaddr(item[1])
            elif item[0] == PARSED_ZIGBEE_KEY:
                # Returned item is a Zigbee Key
                key_translate = {ord(i) : None for i in '-:'}
                key_str = item[1].translate(key_translate)
                add_zb_key(key_str)
            elif item[0] == PARSED_ZIGBEE_DIRECT_KEY:
                # Returned item is a Zigbee Direct Key
                add_zd_key(item[1])
            elif COND_ZBCLI_FEATURES and item[0] == PARSED_TCSO_BACKUP:
                add_tcso_backup(item[1])

    if DEV_LOG:
        DEV_LOG.flush()

def test_log_write(msg):
    global IS_MANUAL_COMMANDS
    global LOG_MUTEX, LOG_ACTIVE
    global LOG_TEMP_LIST

    # Check if another thread is writing to the log.
    # Queue this message if it is.
    LOG_MUTEX.acquire()
    if LOG_ACTIVE:
        LOG_TEMP_LIST.append(msg)
        LOG_MUTEX.release()
        return
    LOG_ACTIVE = True
    LOG_MUTEX.release()

    # We have a lock on the log (LOG_ACTIVE). Write to the log file.
    _test_log_write(msg)

    # Check for log messages queued from other threads while we were
    # writing to the log file.
    LOG_MUTEX.acquire()
    while len(LOG_TEMP_LIST) != 0:
        msg = LOG_TEMP_LIST[0]
        del LOG_TEMP_LIST[0]
        _test_log_write(msg)
    LOG_ACTIVE = False
    LOG_MUTEX.release()

def signal_handler(sig, frame):
    add_capture_file_script()
    print("TEST INTERRUPTED")
    cleanup()
    sys.exit()

def print_header():
    # Startup Notice
    print("chai-tool, Copyright (C) 2020 - 2023, Exegin Technologies Limited")
    print("This program comes with ABSOLUTELY NO WARRANTY. This is free software,")
    print("and you are welcome to redistribute it under certain conditions")

    if COND_DISPLAY_LICENSE:
        print("THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY")
        print("APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT")
        print("HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT")
        print("WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT")
        print("LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A")
        print("PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF")
        print("THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME")
        print("THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.")
        print("")
        print("You may convey verbatim copies of the Program's source code as you")
        print("receive it, in any medium, provided that you conspicuously and")
        print("appropriately publish on each copy an appropriate copyright notice; keep")
        print("intact all notices stating that this License applies to the code; keep")
        print("intact all notices of the absence of any warranty; and give all")
        print("recipients a copy of this License along with the Program.")
        print("")
        print("You may charge any price or no price for each copy that you convey, and")
        print("you may offer support or warranty protection for a fee.")
        print("")
        input("Press any key to continue.\n")

def parse_arguments():
    global LOG_LABEL
    global DISABLED_TYPES
    global COND_NO_CAPTURE
    global COND_NO_CHECKS
    global COND_NO_PAUSE
    global COND_FAST
    global COND_DISPLAY_LICENSE
    global COND_ADD_INFO
    global COND_USE_SCRIPT_LIST
    global COND_CONVERT_R22_GU_CMDS
    global CUSTOM_INTERFACE_LIST
    global SCRIPTS_PATH
    global CONFIG_FILE_PATH
    global SNIFFER_CHANNEL_OVERRIDE
    global DEVICES_CONFIG_OVERRIDE
    global ZDD_EUI48_OVERRIDE
    global PICS_STRING

    parser = argparse.ArgumentParser()
    parser.add_argument('config_path', nargs='?', type=pathlib.Path, help='Specify the path to the chai-tool configuration file')
    parser.add_argument('script_path', nargs='?', type=pathlib.Path, help='Specify the path to a test script or directory of test scripts')
    parser.add_argument('-a', '--add_info_menu', action="store_true", help='Enable menu to add info for disabled devices')
    parser.add_argument('-c', '--channel', action="append", help='Override configured sniffer channels for this test run')
    parser.add_argument('-d', '--disable', action="append", help='Specify a device prompt that will have device commands disabled (or type dut/gu)')
    parser.add_argument('-e', '--devices', action="store", help='Override devices configuration file for this test run')
    parser.add_argument('-f', '--fast', action='store_true', help='Used to speed up testing against disabled devices')
    parser.add_argument('-i', '--interface', action='append', help='Add capture interface to list. Will be combined with interfaces of configured sniffers, if any.')
    parser.add_argument('-l', '--label', action="store", help='Specify a string to include in log directory (usually DUT description)')
    parser.add_argument('-L', '--license', action="store_true", help='Used to display license, warranty and redistribution information')
    parser.add_argument('-n', '--no_capture', action="store_true", help='Used to disable sniffing and tshark checks')
    parser.add_argument('-o', '--no_checks', action="store_true", help='Used to disable tshark checks, but leave sniffing enabled')
    parser.add_argument('-p', '--no_pause', action="store_true", help='Used to disable #script: pause command, unless "hard" is used as additional pause parameter')
    parser.add_argument('-P', '--pics', action="store", help='Specify the PICS string used to determine the device under test for the current test run')
    parser.add_argument('-r', '--replace', action="append", help='Specify a string replacement in the test scripts STRING,REPLACEMENT')
    parser.add_argument('-R', '--r22_gu', action="store_true", help='Convert GU commands to use old zbcli r22 syntax')
    parser.add_argument('-s', '--script_list', action="store_true", help='Used when "path" points to file containing a list of script paths and replacements' + \
                        ' separated by spaces. Each line in the file has syntax: /path/to/script_n STRING_1,REPLACEMENT_1 STRING_2,REPLACEMENT_2')
    parser.add_argument('-z', '--zdd_eui48', action="store", help='Override configured ZDD_EUI48 for your BLE sniffer. Syntax: AB:CD:EF:12:34:56')
    parsed_args = parser.parse_args()

    try:
        CONFIG_FILE_PATH = parsed_args.config_path.resolve()
        assert(os.path.isfile(CONFIG_FILE_PATH))
    except:
        print("Invalid config file path. Use -h or --help to print argument information")
        sys.exit()

    try:
        SCRIPTS_PATH = parsed_args.script_path.resolve()
        assert(os.path.exists(SCRIPTS_PATH))
    except:
        print("Invalid script path. Use -h or --help to print argument information")
        sys.exit()

    if parsed_args.channel: SNIFFER_CHANNEL_OVERRIDE = parsed_args.channel
    if parsed_args.disable: DISABLED_TYPES = parsed_args.disable
    if parsed_args.devices: DEVICES_CONFIG_OVERRIDE = parsed_args.devices
    if parsed_args.interface: CUSTOM_INTERFACE_LIST = parsed_args.interface
    if os.path.isfile(SCRIPTS_PATH):
        # If scripts path is a file rather than a directory, add file name to log label
        LOG_LABEL += "_" + os.path.splitext(os.path.split(SCRIPTS_PATH)[1])[0]
    if parsed_args.label: LOG_LABEL += "_" + parsed_args.label
    if parsed_args.pics: PICS_STRING = parsed_args.pics
    if parsed_args.replace:
        r_list = parsed_args.replace
        for item in r_list:
            r_split = item.split(',')
            if len(r_split) == 2:
                USER_REPLACEMENTS_CMD_LINE[r_split[0]] = r_split[1]
            else:
                print("Invalid replacement syntax: " + item)
                sys.exit()
    if parsed_args.zdd_eui48:
        ZDD_EUI48_OVERRIDE = parsed_args.zdd_eui48
        if len(ZDD_EUI48_OVERRIDE) != 17:
            print("Invalid ZDD EUI48 format for override value. Syntax: AB:CD:EF:12:34:56")
            sys.exit()

    COND_NO_CAPTURE = parsed_args.no_capture
    COND_NO_CHECKS = parsed_args.no_checks
    COND_NO_PAUSE = parsed_args.no_pause
    COND_FAST = parsed_args.fast
    COND_DISPLAY_LICENSE = parsed_args.license
    COND_ADD_INFO = parsed_args.add_info_menu
    COND_USE_SCRIPT_LIST = parsed_args.script_list
    COND_CONVERT_R22_GU_CMDS = parsed_args.r22_gu

def init_test_output():
    global OUTPUT_DIR

    if not os.path.exists("out"): os.mkdir("out")
    if LOG_LABEL:
        # Update output directories if end string is passed in
        OUTPUT_DIR = OUTPUT_DIR.strip("/") + LOG_LABEL + "/"
    os.mkdir(OUTPUT_DIR)
    init_logger()

def get_tc_time_elapsed():
    return (datetime.now() - TEST_CASE_START_TIME)

def prompt_is_dut(prompt):
    if DUT_OVERRIDE:
        if prompt == DUT_OVERRIDE:
            return True
    else:
        for prefix in DUT_PREFIXES:
            if prompt.startswith(prefix):
                return True
    return False

def prompt_is_gu(prompt):
    if DUT_OVERRIDE:
        if prompt != DUT_OVERRIDE:
            return True
    else:
        for prefix in GU_PREFIXES:
            if prompt.startswith(prefix):
                return True
    return False

class nwkaddr_thread(threading.Thread):
    def __init__ (self):
        threading.Thread.__init__(self)
        self.daemon = True
        self.kill = False

    def run(self):
        global EXPECTED_DEV_ANNCE

        framenum = 0
        prev_framenum = -1
        prev_list_len = 0
        while not self.kill:
            if framenum != prev_framenum or prev_list_len != len(ZIGBEE_KEYS):
                cmd = 'tshark -2 -r ' + SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + '.pcapng ' + \
                    '-Y "zbee_aps.zdp_cluster == 0x0013 && frame.number > ' + str(framenum) + \
                    '" -T fields -e frame.number -e zbee_zdp.nwk_addr -e zbee_zdp.ext_addr'
                for key in ZIGBEE_KEYS:
                    cmd += " -o 'uat:zigbee_pc_keys:\"%s\",\"Normal\",\"foo\"'" % key
                prev_framenum = framenum
                prev_list_len = len(ZIGBEE_KEYS)
            output = get_status_output(cmd)[1]
            output_list = output.splitlines()
            if len(output_list) > 0:
                for line in output_list:
                    fn, nwkaddr, extaddr = line.split("\t")
                    extaddr = "0x" + (extaddr.replace(':', '')).lower()
                    nwkaddr = nwkaddr.lower()
                    if EXPECTED_DEV_ANNCE:
                        logging.info_highlight("New addressing from joining device %s (via device announcement packet): %s, %s (extaddr, nwkaddr)" \
                                               % (EXPECTED_DEV_ANNCE, extaddr, nwkaddr))
                        SCRIPT_DEVICES[EXPECTED_DEV_ANNCE]["EXTADDR"] = extaddr
                        SCRIPT_DEVICES[EXPECTED_DEV_ANNCE]["NWKADDR"] = nwkaddr
                        EXPECTED_DEV_ANNCE = None
                        DEV_ANNCE_EVENT.set()
                    else:
                        for device in SCRIPT_DEVICES:
                            if SCRIPT_DEVICES[device]["EXTADDR"] == extaddr and SCRIPT_DEVICES[device]["NWKADDR"] != nwkaddr:
                                logging.info_highlight("New network address (via device announcement packet): " + device + "=" + nwkaddr)
                                SCRIPT_DEVICES[device]["NWKADDR"] = nwkaddr
                    framenum = int(fn)

def read_config():
    global RETRY_COUNT
    global MORE_DEVICE_LOGS
    global TIMEOUT_COMMAND_DEFAULT
    global TIMEOUT_OVERRIDE
    global ZIGBEE_KEYS_DEFAULT
    global ZD_KEYS_DEFAULT
    global UNSUPPORTED_GU_CMDS
    global UNSUPPORTED_DUT_CMDS
    global SOURCE_CMD_DIR
    global COND_ZBCLI_FEATURES
    global COND_PARSE_ZBCLI_SCRIPT_CHANNELS
    global EXPECTED_DEV_ANNCE_CMDS
    global DEVICES_MODULE_PATH
    global SNIFFERS_MODULE_PATH
    global LOG_PARSER_FILEPATH
    global SOURCE_CBKE_CERT_PATH
    global CHANNEL_CONFIG_PATH
    global DEVICES_MODULE
    global SNIFFERS_MODULE
    global LOG_PARSER_MODULE
    global CHANNEL_DICT

    try:
        with open(CONFIG_FILE_PATH, 'r') as f:
            config = json.load(f)
    except:
        print("Could not open configuration file '%s'. Please check it follows json file format." % str(CONFIG_FILE_PATH))
        sys.exit()

    devices_config_path = ""
    for tag in config:
        if tag == "DEVICES_CONFIG_PATH":
            devices_config_path = config[tag]
        elif tag == "SOURCE_CMD_DIR_PATH":
            SOURCE_CMD_DIR = config[tag]
        elif tag == "DEVICES_MODULE_PATH":
            DEVICES_MODULE_PATH = config[tag]
        elif tag == "SNIFFERS_MODULE_PATH":
            SNIFFERS_MODULE_PATH = config[tag]
        elif tag == "LOG_PARSER_MODULE_PATH":
            LOG_PARSER_FILEPATH = config[tag]
        elif tag == "SOURCE_CBKE_CERT_PATH":
            SOURCE_CBKE_CERT_PATH = config[tag]
        elif tag == "CHANNEL_CONFIG_PATH":
            CHANNEL_CONFIG_PATH = config[tag]
        elif tag == "RETRY_COUNT":
            RETRY_COUNT = config[tag]
        elif tag == "TIMEOUT_DEFAULT":
            TIMEOUT_COMMAND_DEFAULT = config[tag]
        elif tag == "ZBCLI_FEATURES":
            COND_ZBCLI_FEATURES = config[tag]
        elif tag == "PARSE_ZBCLI_SCRIPT_CHANNELS":
            COND_PARSE_ZBCLI_SCRIPT_CHANNELS = config[tag]
        elif tag == "EXPECTED_DEV_ANNCE_CMDS":
            EXPECTED_DEV_ANNCE_CMDS = config[tag]
        elif tag == "MORE_DEVICE_LOGS":
            MORE_DEVICE_LOGS = True if config[tag] else False
        elif tag == "TIMEOUT_OVERRIDE":
            TIMEOUT_OVERRIDE = config[tag]
        elif tag == "ZIGBEE_KEYS":
            ZIGBEE_KEYS_DEFAULT = config[tag]
        elif tag == "ZD_KEYS":
            ZD_KEYS_DEFAULT = config[tag]
        elif tag == "UNSUPPORTED_GU_CMDS":
            UNSUPPORTED_GU_CMDS = config[tag]
        elif tag == "UNSUPPORTED_DUT_CMDS":
            UNSUPPORTED_DUT_CMDS = config[tag]

    keys_edited = []
    for key in ZIGBEE_KEYS_DEFAULT:
        key = key.replace(":", "")
        if len(key) == 32:
            keys_edited.append(key)
        else:
            print("config.json error: Zigbee key '%s' should be 32 symbols long" % key)
            sys.exit()
    ZIGBEE_KEYS_DEFAULT = keys_edited

    keys_edited = []
    for key in ZD_KEYS_DEFAULT:
        zd_strings = key.split(",")
        zd_strings = [entry.strip() for entry in zd_strings]
        if len(zd_strings) == 3 and \
            len(zd_strings[0]) == 16 and \
            len(zd_strings[1]) == 16 and \
            len(zd_strings[2]) == 32:
            keys_edited.append(zd_strings)
        else:
            print("Zigbee Direct key has incorrect length or format: '%s'" % key)
            sys.exit()
    ZD_KEYS_DEFAULT = keys_edited

    if DEVICES_MODULE_PATH:
        try:
            spec = importlib.util.spec_from_file_location("devices_module", DEVICES_MODULE_PATH)
            DEVICES_MODULE = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(DEVICES_MODULE)
        except:
            print("Failed to import devices module from path=%s. Please DEVICES_MODULE_PATH in config.json." % DEVICES_MODULE_PATH)
            sys.exit()

    if SNIFFERS_MODULE_PATH:
        try:
            spec = importlib.util.spec_from_file_location("sniffers_module", SNIFFERS_MODULE_PATH)
            SNIFFERS_MODULE = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(SNIFFERS_MODULE)
        except:
            print("Failed to import sniffer module from path=%s. Please SNIFFERS_MODULE_PATH in config.json." % SNIFFERS_MODULE_PATH)
            sys.exit()

    if LOG_PARSER_FILEPATH:
        try:
            spec = importlib.util.spec_from_file_location("log_parser_module", LOG_PARSER_FILEPATH)
            LOG_PARSER_MODULE = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(LOG_PARSER_MODULE)
        except:
            print("Failed to import sniffer module from path=%s. Please LOG_PARSER_FILEPATH in config.json." % LOG_PARSER_FILEPATH)
            sys.exit()

    if DEVICES_CONFIG_OVERRIDE:
        devices_config_path = DEVICES_CONFIG_OVERRIDE

    try:
        assert(os.path.isfile(devices_config_path))
        with open(devices_config_path, 'r') as f:
            devices_config = json.load(f)
    except:
        print("Could not open devices configuration file '%s'. Please check it exists and is in json file format." % devices_config_path)
        sys.exit()

    try:
        assert(os.path.exists(SOURCE_CMD_DIR))
    except:
        print("Source command directory '%s' does not exist. Please check config file %s." % (SOURCE_CMD_DIR, CONFIG_FILE_PATH))
        sys.exit()

    if SOURCE_CBKE_CERT_PATH:
        try:
            assert(os.path.exists(SOURCE_CBKE_CERT_PATH))
            for root, dirs, files in os.walk(SOURCE_CBKE_CERT_PATH):
                for basename in files:
                    path = os.path.join(root, basename)
                    if not basename.startswith("cbke_"):
                        continue
                    abspath = os.path.abspath(path)
                    euistr = basename.split("_")[2].strip()
                    euistr = euistr.split(".txt")[0].strip()
                    euistr = "0x%s" % (euistr)
                    # Last value becomes True when in use
                    CBKE_CERT_LIST.append([abspath, euistr, False])
                # Only care about base folder
                break
        except:
            print("CBKE cert source directory '%s' does not exist. Please check config file %s." % (SOURCE_CBKE_CERT_PATH, CONFIG_FILE_PATH))
            sys.exit()

    if CHANNEL_CONFIG_PATH:
        CHANNEL_DICT = channel_config_read(CHANNEL_CONFIG_PATH)
        if CHANNEL_DICT == None:
            print("Failed to process CHANNEL_CONFIG_PATH '%s'. Please check config file '%s'." % (CHANNEL_CONFIG_PATH, CONFIG_FILE_PATH))
            sys.exit()

    for type in devices_config:
        if type == "SNIFFER":
            for cfg in devices_config[type]:
                if len(cfg) < 5:
                    print("Device missing sniffer parameters (APP_PATH, DEV_PATH, CHANNEL, PAGE, TSHARK_INTERFACE)")
                    print("See documentation for configuration details.")
                    sys.exit()

                if cfg["APP_PATH"][0] != "/": cfg["APP_PATH"] = os.getcwd() + "/" + cfg["APP_PATH"]
                SNIFFER_CFGS.append(cfg)

        if type == "BLE_SNIFFER":
            for cfg in devices_config[type]:
                if len(cfg) < 4:
                    print("Device missing sniffer parameters (APP_PATH, DEV_PATH, ZDD_EUI48, TSHARK_INTERFACE)")
                    print("See documentation for configuration details.")
                    sys.exit()
                if cfg["APP_PATH"][0] != "/": cfg["APP_PATH"] = os.getcwd() + "/" + cfg["APP_PATH"]
                if ZDD_EUI48_OVERRIDE: cfg["ZDD_EUI48"] = ZDD_EUI48_OVERRIDE
                if len(cfg["ZDD_EUI48"]) != 17:
                    print("Invalid ZDD EUI48 value in devices configuration file. Syntax: AB:CD:EF:12:34:56")
                    sys.exit()
                BLE_SNIFFER_CFGS.append(cfg)

        elif type in ("SERIAL", "SSH"):
            for cfg in devices_config[type]:
                if "TAG" not in cfg: cfg["TAG"] = ""
                if "SUCCESS_STRING" not in cfg: cfg["SUCCESS_STRING"] = "COMMAND=SUCCESS"
                if "FAIL_STRING" not in cfg: cfg["FAIL_STRING"] = "COMMAND=FAIL"
                cfg["ROLE"] = [x.strip() for x in cfg["ROLE"].split(',') if x]
                cfg["TYPE"] = type
                cfg["ID"] = ""

                if type == "SERIAL":
                    cfg["2.4GHZ"] = True if ("2.4GHZ" in cfg and (cfg["2.4GHZ"] == "YES" or cfg["2.4GHZ"] == "Y")) else False
                    cfg["SUBGHZ"] = True if ("SUBGHZ" in cfg and (cfg["SUBGHZ"] == "YES" or cfg["SUBGHZ"] == "Y")) else False
                    if len(cfg) < 9:
                        print("Device missing serial parameters (TAG [Opt.], ROLE, PATH, 2.4GHZ [Opt.], SUBGHZ [Opt.],")
                        print("    SUCCESS_STRING [Opt.], FAIL_STRING [Opt.])")
                        print("See documentation for configuration details.")
                        sys.exit()

                elif type == "SSH":
                    for opt_arg in ("PASS", "SSHKEY_PATH", "CLI_ATTACH_2.4GHZ", "CLI_ATTACH_SUBGHZ"):
                        if opt_arg not in cfg: cfg[opt_arg] = ""
                    if len(cfg) < 14:
                        print("Device missing SSH parameters (TAG [Opt.], ROLE, IP, PORT, USERNAME, PASS [Opt.],")
                        print("    SSHKEY_PATH [Opt.], CLI_START, CLI_ATTACH_2.4GHZ [Opt.], CLI_ATTACH_SUBGHZ [Opt.],")
                        print("    SUCCESS_STRING [Opt.], FAIL_STRING [Opt.])")
                        print("See documentation for configuration details.")
                        sys.exit()

                DEVICE_CFGS.append(cfg)

    if not COND_NO_CAPTURE and not SNIFFER_CFGS and not BLE_SNIFFER_CFGS and not CUSTOM_INTERFACE_LIST:
        print("No sniffers or capture interfaces were configured. Please use -n argument if you with to run without sniffing or tshark checks.")
        sys.exit()

def append_cmd_list(cmd_list, script_path, lno_pre=""):
    global ACTIVE_DEVICE
    global DUT_OVERRIDE

    if os.path.isfile(script_path.strip()):
        script_file = open(script_path.strip(), "r")
        multiline = ""

        # Process each line of the script file
        for i,line in enumerate(script_file):
            lno = lno_pre + str(i+1)
            line = line.strip()
            # Multiline handling
            if line.endswith("\\"):
                if multiline == "":
                    multiline = line[:-1]
                else:
                    multiline += line[:-1].lstrip()
                continue
            if multiline != "":
                multiline += line.lstrip()
                line = multiline
                multiline = ""

            line_with_replacement = line_replacements(line)
            parameters = line_with_replacement.split(" ")
            if parameters[0] in SCRIPT_ESCAPE_COMMANDS or (line[0:1] !="#" and line != ""):
                if len(parameters) > 1:
                    if parameters[0] == SCRIPT_ESCAPE_DEVICE_COMMAND:
                        ACTIVE_DEVICE = parameters[1]
                        R22_GU_UTIL.reset()
                        if ACTIVE_DEVICE not in SCRIPT_DEVICES:
                            add_script_device(ACTIVE_DEVICE)
                    elif parameters[0] == SCRIPT_ESCAPE_HW_COMMAND and parameters[1] == HW_COMMAND_RESET:
                        R22_GU_UTIL.reset()
                    elif parameters[0] == SCRIPT_ESCAPE_SCRIPT_COMMAND:
                        handle_script_command_prelim(parameters)
                    elif parameters[0] == SCRIPT_ESCAPE_PICS_DUT_DEVICES:
                        # Note: this command should always come before any '#script: add-device' or '#device commands'
                        pics_dut_parameters = "".join(parameters[1:]).split(",")
                        if len(pics_dut_parameters) == 2 and PICS_STRING == pics_dut_parameters[0]:
                            DUT_OVERRIDE = pics_dut_parameters[1]

                    # zbcli-specific: Use default vpan replacements
                    elif COND_ZBCLI_FEATURES and parameters[0] == SCRIPT_ESCAPE_DEFAULT_REPLACE_COMMAND:
                        r_split = parameters[1].split(",")
                        if r_split[0] not in USER_REPLACEMENTS:
                            USER_REPLACEMENTS[r_split[0]] = r_split[1]
                    # zbcli-specific: 'source' has same function as '#source:'
                    elif (parameters[0] == SCRIPT_ESCAPE_SOURCE_COMMAND or \
                        (COND_ZBCLI_FEATURES and parameters[0] == "source")):
                        if not append_cmd_list(cmd_list, os.path.join(SOURCE_CMD_DIR, parameters[1]), lno + ","):
                            return False
                        continue

                # Disable script if there are unsupported cmds for DUT or GU
                if prompt_is_dut(ACTIVE_DEVICE):
                    for string in UNSUPPORTED_DUT_CMDS:
                        if line_with_replacement.startswith(string):
                            return False
                elif prompt_is_gu(ACTIVE_DEVICE):
                    for string in UNSUPPORTED_GU_CMDS:
                        if line_with_replacement.startswith(string):
                            return False
                    if COND_CONVERT_R22_GU_CMDS:
                        for c in R22_GU_UTIL.convert_cmd(line):
                            cmd_list.append((lno, c))
                        continue

                # append line without replacement, in case replacement changes during execution
                cmd_list.append((lno, line.rstrip("\n")))

        script_file.close()
        return True
    else:
        if ACTIVE_DEVICE in DISABLED_TYPES or \
            (prompt_is_dut(ACTIVE_DEVICE) and not set(DUT_PREFIXES).isdisjoint(DISABLED_TYPES)) or \
            (prompt_is_gu(ACTIVE_DEVICE) and not set(GU_PREFIXES).isdisjoint(DISABLED_TYPES)):
            logging.warning("Failed to source script file for disabled device (%s)" % ACTIVE_DEVICE)
            return True
        logging.error("Error sourcing the following script file: " + script_path)
        return False

def cbke_get_next_eui(desc):
    global CBKE_CERT_LIST

    for i in range(len(CBKE_CERT_LIST)):
        (path, euistr, used) = CBKE_CERT_LIST[i]
        if used:
            continue

        # Find the right cert
        if desc == "CBKEv1v2":
            if path.find("cbke_v1v2_") == -1:
                continue
        elif desc == "CBKEv1":
            if path.find("cbke_v1_") == -1:
                continue
        elif desc == "CBKEv2":
            if path.find("cbke_v2_") == -1:
                continue
        else:
            logging.error("Error, invalid or unknown CBKE cert description: %s\n" % (desc))
            return None, None

        cbke_cert_cmds = []
        append_cmd_list(cbke_cert_cmds, path)

        # Flag as used
        CBKE_CERT_LIST[i][2] = True

        # Return this entry, including EUI64 and path to the cert file
        return euistr, cbke_cert_cmds

    return None, None

def init_test_scripts():
    global SCRIPT_LIST

    test_script_paths = []
    if os.path.isfile(SCRIPTS_PATH):
        if COND_USE_SCRIPT_LIST:
            # -s argument signals that 'path' argument points to a file containing
            # a list of script paths and replacements
            script_list_file = open(SCRIPTS_PATH, "r")
            for line in script_list_file:
                args = [a.strip() for a in line.split(" ")]
                path = args[0]
                if not os.path.isfile(path):
                    print("Invalid path in script list: " + path)
                    sys.exit()
                replacements = {}
                replacements.update(USER_REPLACEMENTS_CMD_LINE)
                for r in args[1:]:
                    r_split = [v.strip() for v in r.split(',')]
                    if len(r_split) == 2:
                        replacements[r_split[0]] = r_split[1]
                    else:
                        print("Invalid replacement syntax in script list: " + r)
                        sys.exit()
                # Initialize SCRIPT_LIST with data from script list file
                SCRIPT_LIST[os.path.splitext(str(os.path.split(path)[1]))[0]] = \
                    {SCRIPT_DICT_CMDS: [], \
                    SCRIPT_DICT_PATH: path, \
                    SCRIPT_DICT_PASS: False, \
                    SCRIPT_DICT_CAUSE_OF_FAIL: "", \
                    SCRIPT_DICT_REPLACEMENTS: replacements}
        else:
            # 'path' argument points to single script file
            test_script_paths = [str(SCRIPTS_PATH)]
    else:
        # 'path' argument points to a directory of script files
        test_script_paths = sorted([os.path.join(SCRIPTS_PATH, f) \
            for f in os.listdir(SCRIPTS_PATH) if os.path.isfile(os.path.join(SCRIPTS_PATH, f))])

    # Initialize SCRIPT_LIST
    for path in test_script_paths:
        SCRIPT_LIST[os.path.splitext(str(os.path.split(path)[1]))[0]] = \
            {SCRIPT_DICT_CMDS: [], \
            SCRIPT_DICT_PATH: path, \
            SCRIPT_DICT_PASS: False, \
            SCRIPT_DICT_CAUSE_OF_FAIL: "", \
            SCRIPT_DICT_REPLACEMENTS: dict(USER_REPLACEMENTS_CMD_LINE)}

    if len(SCRIPT_LIST) == 0:
        print("No valid scripts found at path: " + str(SCRIPTS_PATH))
        sys.exit()

def channel_config_read(path):
    # Dictionary items:
    #   "CHANNEL_PAGE"
    #   "CHANNEL_MASK"
    #   "CHANNEL_MASK_WITH_PAGE"
    #   "CHANNEL"
    config_dict = {}

    with open(path, "r") as f:
        mydata = f.read()

    page_val = None
    mask_val = None

    for line in mydata.splitlines():
        line = line.strip()
        if line == "":
            continue
        if line.startswith("#"):
            continue

        if line.startswith("CHANNEL_PAGE"):
            if page_val is not None:
                return None
            val = int(line.split("=")[1].strip())
            if val > 31:
                # Invalid page
                return None
            # Save the page in string format
            config_dict["CHANNEL_PAGE"] = "%d" % (val)
            page_val = val

        elif line.startswith("CHANNEL_MASK"):
            if page_val is None:
                return None
            if mask_val is not None:
                return None
            val_str = line.split("=")[1].strip()
            if val_str.startswith("0x"):
                val_str = val_str[2:]
            val = int(val_str, 16)
            if val == 0:
                # Invalid mask
                return None
            if val > 0x07ffffff:
                # Invalid mask
                return None
            # Save the mask in string format
            config_dict["CHANNEL_MASK"] = "0x%08x" % (val)
            mask_val = val

        elif line.startswith("CHANNEL_BDB_PRI_MASK"):
            val_str = line.split("=")[1].strip()
            if val_str.startswith("0x"):
                val_str = val_str[2:]
            val = int(val_str, 16)
            if val == 0:
                # Invalid mask
                return None
            # EXEGIN - allow page bits?
            if val > 0x07ffffff:
                # Invalid mask
                return None
            # Save the mask in string format
            config_dict["CHANNEL_BDB_PRI_MASK"] = "0x%08x" % (val)

        elif line.startswith("CHANNEL_BDB_SEC_MASK"):
            val_str = line.split("=")[1].strip()
            if val_str.startswith("0x"):
                val_str = val_str[2:]
            val = int(val_str, 16)
            if val == 0:
                # Invalid mask
                return None
            # EXEGIN - allow page bits?
            if val > 0x07ffffff:
                # Invalid mask
                return None
            # Save the mask in string format
            config_dict["CHANNEL_BDB_SEC_MASK"] = "0x%08x" % (val)


    if page_val is None or mask_val is None:
        return None

    # Generate the channel mask with the page bits included
    mask_with_page = mask_val | (page_val << 27)
    config_dict["CHANNEL_MASK_WITH_PAGE"] = "0x%08x" % (mask_with_page)

    return config_dict

def init_sniffers():
    global SNIFFER_THREADS
    global TSHARK_MAIN

    if COND_NO_CAPTURE:
        return

    if (SNIFFER_CFGS or BLE_SNIFFER_CFGS) and not SNIFFERS_MODULE:
        logging.error("Error: Cannot initialize configured sniffer devices because SNIFFERS_MODULE_PATH is not set. " + \
                      "Please check config.json or run chai-tool.py with the -n option to disable sniffing.")
        sys.exit()

    if SNIFFER_CHANNEL_OVERRIDE and len(SNIFFER_CHANNEL_OVERRIDE) > len(SNIFFER_CFGS):
        logging.error("Error: Found %d channel override (-c) inputs, but only %d sniffers are configured." % \
                        (len(SNIFFER_CHANNEL_OVERRIDE), len(SNIFFER_CFGS)))
        sys.exit()

    # zbcli-specific: Parse zbcli scripts for channel,page pairs
    if COND_ZBCLI_FEATURES:
        parsed_channel_page_list = []
        for lno,line in SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS]:
            # Search for commands that reference a page and one or more channels
            if "--channels" in line and "--page" in line:
                cs = line.partition("--channels")[2].strip().split(" ")[0]
                p = line.partition("--page")[2].strip().split(" ")[0]
                if p and p.isdigit():
                    for c in cs.split(","):
                        if c and c.isdigit() and (c, p) not in parsed_channel_page_list: parsed_channel_page_list.append((c, p))
            elif "--channel" in line and "--page" in line:
                c = line.partition("--channel")[2].strip().split(" ")[0]
                p = line.partition("--page")[2].strip().split(" ")[0]
                if c and p and c.isdigit() and p.isdigit() and (c, p) not in parsed_channel_page_list: parsed_channel_page_list.append((c, p))
        if len(parsed_channel_page_list) > len(SNIFFER_CFGS):
            logging.warning("WARNING: Found %d channel,page pairs referenced in script, but only %d sniffers are configured." % \
                            (len(parsed_channel_page_list), len(SNIFFER_CFGS)))
        logging.info("CHANNELS,PAGES: " + str(parsed_channel_page_list))

    SNIFFER_THREADS = []
    parsed_channels_covered = []
    for i, cfg in enumerate(SNIFFER_CFGS):
        # Start sniffers
        try:
            channel_page = (cfg["CHANNEL"], cfg["PAGE"])
            if SNIFFER_CHANNEL_OVERRIDE:
                if i == len(SNIFFER_CHANNEL_OVERRIDE):
                    # No more sniffers required for this test script
                    break
                channel_page = (SNIFFER_CHANNEL_OVERRIDE[i], cfg["PAGE"])
            # zbcli-specific: Assign sniffers a channel,page pair using list parsed from scripts
            elif COND_ZBCLI_FEATURES and COND_PARSE_ZBCLI_SCRIPT_CHANNELS:
                if i == len(parsed_channel_page_list):
                    # No more sniffers required for this test script
                    break
                channel_page = parsed_channel_page_list[i]
            logging.info("Starting sniffer device %s on channel %s, page %s... " % (cfg["DEV_PATH"], channel_page[0], channel_page[1]))
            if cfg["TSHARK_INTERFACE"] not in ZB_INTERFACE_LIST: ZB_INTERFACE_LIST.append(cfg["TSHARK_INTERFACE"])
            new_thread = SNIFFERS_MODULE.sniffer_thread(cfg["APP_PATH"], cfg["DEV_PATH"], channel_page[0], channel_page[1], cfg["TSHARK_INTERFACE"])
            new_thread.start()
            if not new_thread.check():
                logging.error("Start failure for sniffer " + cfg["DEV_PATH"])
                sys.exit()
            SNIFFER_THREADS.append(new_thread)
            if channel_page in parsed_channel_page_list and channel_page not in parsed_channels_covered:
                parsed_channels_covered.append(channel_page)
        except:
            logging.error("Failed sniffer initialization for device " + cfg["DEV_PATH"])
            sys.exit()

    if COND_ZBCLI_FEATURES and len(parsed_channel_page_list) > len(parsed_channels_covered):
        logging.warning("WARNING: Found %d channel,page pairs referenced in script, but only %d of these are captured by sniffers." % \
                        (len(parsed_channel_page_list), len(parsed_channels_covered)))

    BLE_SNIFFER_THREADS = []
    for cfg in BLE_SNIFFER_CFGS:
        try:
            if cfg["TSHARK_INTERFACE"] not in BLE_INTERFACE_LIST: BLE_INTERFACE_LIST.append(cfg["TSHARK_INTERFACE"])
            new_thread = SNIFFERS_MODULE.ble_sniffer_thread(cfg["APP_PATH"], cfg["DEV_PATH"], cfg["ZDD_EUI48"], cfg["TSHARK_INTERFACE"])
            new_thread.start()
            if not new_thread.check():
                logging.error("Start failure for BLE sniffer " + cfg["DEV_PATH"])
                sys.exit()
            BLE_SNIFFER_THREADS.append(new_thread)
        except:
            logging.error("Failed ble sniffer initialization for device " + cfg["DEV_PATH"])
            sys.exit()

    if ZB_INTERFACE_LIST or BLE_INTERFACE_LIST or CUSTOM_INTERFACE_LIST:
        # Create output file only if there are tshark interfaces to read from
        tshark_args = "tshark"
        output_file = SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".pcapng"
        # Combine packets from all interfaces into output pcapng file
        for interface in ZB_INTERFACE_LIST:
            tshark_args += " -i " + interface
        for interface in BLE_INTERFACE_LIST:
            tshark_args += " -i " + interface
        for interface in CUSTOM_INTERFACE_LIST:
            tshark_args += " -i " + interface
        tshark_args += " -w " + output_file
        TSHARK_MAIN = subprocess.Popen(shlex.split(tshark_args), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)

        # Check output file was created
        time_now = time.time()
        while time.time() < (time_now + float(10)) and not os.path.isfile(output_file):
            time.sleep(1)
        if not os.path.isfile(output_file):
            logging.error("tshark process failed to start, check your sniffer output interface(s):")
            logging.error(TSHARK_MAIN.stderr.read().decode('UTF-8'))
            sys.exit()

def handle_script_command_prelim(parameters):
    global SCRIPT_FAIL

    if parameters[1] == SCRIPT_COMMAND_ADD_DEVICE and len(parameters) > 2:
        add_device_parameters = "".join(parameters[2:]).split(",")
        device_id = add_device_parameters[0]
        if device_id not in SCRIPT_DEVICES:
            add_script_device(device_id)
        if len(add_device_parameters) > 1:
            if COND_ZBCLI_FEATURES and add_device_parameters[1].startswith("CBKE"):
                # Take extended address from CBKE pool
                extaddr, cbke_cert_cmds = cbke_get_next_eui(add_device_parameters[1])
                if extaddr is None:
                    logging.error("Error, failed to find CBKE cert file for '%s'. Check SOURCE_CBKE_CERT_PATH value in %s is correct" % \
                                  (device_id, CONFIG_FILE_PATH))
                    SCRIPT_FAIL = True
                    return
                SCRIPT_DEVICES[device_id]["EXTADDR"] = extaddr.lower()
                SCRIPT_DEVICES[device_id]["CBKE_CERT_CMDS"] = cbke_cert_cmds
            else:
                # Initialize device with an extended address
                extaddr = add_device_parameters[1]
                SCRIPT_DEVICES[device_id]["EXTADDR"] = extaddr.lower()

        if len(add_device_parameters) > 2:
            other_args = add_device_parameters[2:]
            pages = []
            for arg in other_args:
                if arg.startswith('p'):
                    pages.append(int(arg[1:]))
                if COND_ZBCLI_FEATURES and arg.startswith('sz='):
                    # Network Neighbor table sizes
                    if "sz=nnt" in arg:
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = int(arg.strip("sz=nnt"))
                    # Stack table sizes
                    if arg == "sz=zc_sml":
                        # EXEGIN - A small ZC? Is this even useful to test?
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 32000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 8
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 9
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 8
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 32
                    elif arg == "sz=zc_med":
                        # Typical ZC
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 64000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 100
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 100
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 9
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 100
                    elif arg == "sz=zc_lrg":
                        # Large network ZC
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 120000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 100
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 1000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 1000

                    ### Router Sizes
                    elif arg == "sz=zr_med":
                        # Typical ZR
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 64000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 100
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 9
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 8
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 32

                    elif arg == "sz=zr_lrg":
                        # Typical ZR
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 120000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 100
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 9
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 8
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 32

                    ### End-Device Sizes
                    elif arg == "sz=zed_sml":
                        # Typical SED
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 8000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 8
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 0
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 0
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 4
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 4

                    elif arg == "sz=zed_med":
                        # ZED with more memory
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_HEAP"] = 16000
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_NNT"] = 16
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ROUTE"] = 0
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_ADDR"] = 32
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_BTT"] = 0
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_RREQ"] = 4
                        SCRIPT_DEVICES[device_id]["DEVICE_DICT_SZ_LINK"] = 4

            if pages:
                if 0 in pages: SCRIPT_DEVICES[device_id]["2.4GHZ_REQ"] = True
                for page in (23, 24, 25, 26, 27, 28, 29, 30, 31):
                    if page in pages: SCRIPT_DEVICES[device_id]["SUBGHZ_REQ"] = True

def device_restore_TCSO_keys(dev_name):
    success = False
    logging.info_highlight("Sending 'aps_link_key load' to restore TCSO key info for '%s'\n" \
        % (dev_name))

    for dict in SCRIPT_DEVICES[dev_name]["TCSO_BACKUP"]:
        restore_cmd = "aps_link_key load"

        for key in dict:
            if key in ("passphraseUpdateAllowed", "verifiedFrameCounter"):
                if int(dict[key]):
                    restore_cmd += (" --%s" % key)
            else:
                restore_cmd += (" --%s %s" % (key, str(dict[key])))
        success = send_to_device(dev_name, restore_cmd)
    return success

def dyn_var_mask_replace_add_bits(var_name, add_bits):
    try:
        rplc_val = int(DYNAMIC_REPLACEMENTS[var_name], 16)
        rplc_val |= add_bits
        rplc_str = "0x%08x" % (rplc_val)
        DYNAMIC_REPLACEMENTS[var_name] = rplc_str
    except:
        return False
    else:
        return True

def handle_pause_command(hard_pause):
    global SCRIPT_FAIL
    global ACTIVE_DEVICE

    pause_loop = True
    saved_device = ACTIVE_DEVICE

    if COND_NO_PAUSE:
        pause_loop = hard_pause
    while pause_loop:
        logging.debug_highlight("Test paused. Input 'c' to continue test script, " + \
            "'m' to send manual command to %s, or 's' to switch active device:" % ACTIVE_DEVICE)
        keypress = input()
        while keypress != "":
            if keypress == "c":
                pause_loop = False
                break
            elif keypress == "m":
                if ACTIVE_DEVICE in DISABLED_TYPES:
                    logging.debug_highlight("Active device %s is disabled." % ACTIVE_DEVICE)
                    break
                logging.debug_highlight("Enter a manual command to send to " + ACTIVE_DEVICE + ":")
                input_cmd = input()
                if input_cmd != "":
                    send_to_device(ACTIVE_DEVICE, input_cmd)
                    break
            elif keypress == "s":
                logging.debug_highlight(str(list(SCRIPT_DEVICES.keys())))
                logging.debug_highlight("Enter a device role to switch to (Active: " + ACTIVE_DEVICE + "):")
                input_dev = input()
                if input_dev in SCRIPT_DEVICES:
                    ACTIVE_DEVICE = input_dev
                else:
                    logging.debug_highlight("Invalid device")
                break
            else:
                logging.debug_highlight("Invalid input")
                break

    if ACTIVE_DEVICE != saved_device:
        logging.debug_highlight("Setting active device back to %s" % saved_device)
        ACTIVE_DEVICE = saved_device

def variable_replace_modify(var_name, change):
    if var_name in DYNAMIC_REPLACEMENTS:
        try:
            old_value = int(DYNAMIC_REPLACEMENTS[var_name])
            DYNAMIC_REPLACEMENTS[var_name] = "%d" % (old_value + int(change))
        except:
            logging.error("Failed to modify dynamic replacement")
            return False
        logging.info_highlight("Modified Dynamic replacement: %s = %s" % (var_name, DYNAMIC_REPLACEMENTS[var_name]))
        return True
    elif var_name in USER_REPLACEMENTS:
        try:
            old_value = int(USER_REPLACEMENTS[var_name])
            USER_REPLACEMENTS[var_name] = "%d" % (old_value + int(change))
        except:
            logging.error("Failed to modify user replacement")
            return False
        logging.info_highlight("Modified User replacement: %s = %s" % (var_name, USER_REPLACEMENTS[var_name]))
        return True
    else:
        logging.error("Failed to find replacement: %s" % var_name)
        return False

def handle_script_command(parameters):
    global SCRIPT_FAIL

    if parameters[1] == SCRIPT_COMMAND_VAR_MODIFY:
        var_split = parameters[2].split(",")
        if len(var_split) != 2:
            SCRIPT_FAIL = True
            return
        try:
            var_change = int(var_split[1])
        except:
            logging.error("Failed to convert change=%s to integer" % var_split[1])
            SCRIPT_FAIL = True
            return
        if not variable_replace_modify(var_split[0], var_change):
            SCRIPT_FAIL = True
            return

    elif parameters[1] == SCRIPT_COMMAND_VAR_NEW:
        var_split = parameters[2].split(",")
        if len(var_split) != 2:
            SCRIPT_FAIL = True
            return
        DYNAMIC_REPLACEMENTS[var_split[0]] = var_split[1]
        logging.info_highlight("Added new dynamic replacement: %s = %s" % (var_split[0], DYNAMIC_REPLACEMENTS[var_split[0]]))

    elif parameters[1] == SCRIPT_COMMAND_VAR_MASK_NEW:
        var_split = parameters[2].split(",")

        var_init = "0x00000000"
        if len(var_split) == 1:
            var_name = var_split[0]
        elif len(var_split) == 2:
            var_name = var_split[0]
            var_init = var_split[1]
        else:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-new syntax"
            return

        logging.info_highlight("Added new mask variable {VAR:%s} = '%s'" % (var_name, var_init))
        DYNAMIC_REPLACEMENTS[var_name] = var_init

    elif parameters[1] == SCRIPT_COMMAND_VAR_MASK_ADD_CHANNEL:
        var_split = parameters[2].split(",")
        if len(var_split) != 2:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-channel syntax"
            return
        try:
            channel = int(var_split[1])
        except:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-channel syntax"
            return
        if channel > 27:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-channel syntax"
            return
        new_channel_bit = 1 << channel
        if not dyn_var_mask_replace_add_bits(var_split[0], new_channel_bit):
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-channel syntax"
            return
        logging.info_highlight("Updated mask variable {%s} = '%s'" % (var_split[0], DYNAMIC_REPLACEMENTS[var_split[0]]))

    elif parameters[1] == SCRIPT_COMMAND_VAR_MASK_ADD_PAGE:
        var_split = parameters[2].split(",")
        if len(var_split) != 2:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-page syntax"
            return
        try:
            page = int(var_split[1])
        except:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-page syntax"
            return
        if page > 31:
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-page syntax"
            return
        new_page_bits = page << 27
        if not dyn_var_mask_replace_add_bits(var_split[0], new_page_bits):
            SCRIPT_FAIL = True
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "var-mask-add-page syntax"
            logging.error("var-mask-add-page syntax error")
            return
        logging.info_highlight("Updated mask variable {%s} = '%s'" % (var_split[0], DYNAMIC_REPLACEMENTS[var_split[0]]))

    elif parameters[1] == SCRIPT_COMMAND_ADD_KEY:
        try:
            key,name = parameters[2].split(",")
            key = key.replace(":", "")
            if not add_zb_key(key):
                logging.error("add-key failed")
                SCRIPT_FAIL = True
        except:
            logging.error("add-key failed, format error on %s" % parameters)
            sys.exit()

    elif parameters[1] == SCRIPT_COMMAND_PAUSE:
        hard_pause = False
        if len(parameters) > 2 and parameters[2] == "hard":
            hard_pause = True
        handle_pause_command(hard_pause)

def add_script_device(device):
    SCRIPT_DEVICES[device] = {}
    SCRIPT_DEVICES[device]["ROLE"] = device
    # Note: Role is converted from one of the pre-defined DUT/GU prefixes to
    # either a d or g to allow running multiple test suites with different naming
    # conventions without need to change configuration file. If all scripts used
    # the same naming convention, you could simply use .startswith() to the same
    # effect. (Check if script_device.startswith(type) during assignment)
    if prompt_is_dut(device):
        SCRIPT_DEVICES[device]["ROLE"] = "d"
    if prompt_is_gu(device):
        SCRIPT_DEVICES[device]["ROLE"] = "g"
    SCRIPT_DEVICES[device]["DISABLED"] = False
    # Usually when disabling devices, only one script is run at a time. This
    # means the user can just specify the DUT/GU prefix for that script to
    # disable all DUT/GU's, or specify the whole device name.
    for type in DISABLED_TYPES:
        disable_type = ""
        if type in DUT_PREFIXES: disable_type = 'd'
        if type in GU_PREFIXES: disable_type = 'g'
        if device.startswith(type) or disable_type == SCRIPT_DEVICES[device]["ROLE"]:
            SCRIPT_DEVICES[device]["DISABLED"] = True
    SCRIPT_DEVICES[device]["THREAD"] = None
    SCRIPT_DEVICES[device]["EXTADDR"] = ""
    SCRIPT_DEVICES[device]["NWKADDR"] = ""

    if COND_ZBCLI_FEATURES and DISABLED_TYPES:
        if device.endswith("TC") or device.endswith("ZC"):
            SCRIPT_DEVICES[device]["NWKADDR"] = "0x0000"
        elif device.endswith("DEVx") and not {key:val for key, val in SCRIPT_DEVICES.items() if key.endswith("TC")}:
            SCRIPT_DEVICES[device]["NWKADDR"] = "0x0000"
    SCRIPT_DEVICES[device]["2.4GHZ_REQ"] = False
    SCRIPT_DEVICES[device]["SUBGHZ_REQ"] = False
    SCRIPT_DEVICES[device]["TCSO_BACKUP"] = []
    SCRIPT_DEVICES[device]["CBKE_CERT_CMDS"] = []

# Create device log for current script
def init_script_output():
    global DEV_LOG
    global SCRIPT_RESULTS_DIR
    global TEST_CASE_START_TIME

    count = 1
    dir = dir = OUTPUT_DIR + CURRENT_SCRIPT + "/"
    while os.path.exists(dir):
        count += 1
        dir = OUTPUT_DIR + CURRENT_SCRIPT + "-" + str(count) + "/"
    SCRIPT_RESULTS_DIR = dir
    os.mkdir(SCRIPT_RESULTS_DIR)

    if DEV_LOG: DEV_LOG.close()
    TEST_CASE_START_TIME = datetime.now()
    DEV_LOG = open(SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".log", "w")
    DEV_LOG.write("chai-tool, Copyright (C) 2020 - 2023, Exegin Technologies Limited\n")
    DEV_LOG.write("DEVICE LOG (" + CURRENT_SCRIPT + ")\n\n")

    logging.info("")
    start_new_logger_file(SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + "_chai.log")
    logging.info("#----------------------------------------------------------------------")
    logging.info("# " + CURRENT_SCRIPT)
    logging.info("#----------------------------------------------------------------------")
    if DISABLED_TYPES: logging.info("DISABLED: %s" % DISABLED_TYPES)
    if USER_REPLACEMENTS: logging.info("USER REPLACEMENTS: %s" % USER_REPLACEMENTS)

def add_device_processes():
    global SCRIPT_DEVICES
    global SCRIPT_FAIL

    if len(SCRIPT_DEVICES) == 0:
        logging.error("No device roles found in script: " + CURRENT_SCRIPT)
        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "No device roles found in script"
        SCRIPT_FAIL = True
    elif not SCRIPT_FAIL:
        logging.info("DEVICE IDS: " + str(list(SCRIPT_DEVICES.keys())))

    # Assign each script device a hardware device and communication process
    for script_device in SCRIPT_DEVICES:
        # Check if device failed to start
        if SCRIPT_FAIL: break
        # Do not assign process to disabled devices
        if SCRIPT_DEVICES[script_device]["DISABLED"]: continue

        logging.info("Assigning device to " + script_device + "...")
        for config in DEVICE_CFGS:
            if config["ID"] == "" and ("a" in config["ROLE"] or script_device in config["ROLE"] or SCRIPT_DEVICES[script_device]["ROLE"] in config["ROLE"]):
                if config["TYPE"] == "SERIAL":
                    # Check if the supported bands match
                    if (SCRIPT_DEVICES[script_device]["2.4GHZ_REQ"] and not config["2.4GHZ"]) or \
                        (SCRIPT_DEVICES[script_device]["SUBGHZ_REQ"] and not config["SUBGHZ"]): continue
                    # Try creating a Serial device
                    try:
                        proc = serial_dev(config["PATH"])
                        SCRIPT_DEVICES[script_device]["THREAD"] = dev_thread(proc, script_device, config["SUCCESS_STRING"], config["FAIL_STRING"], \
                                                                              test_log_write, MORE_DEVICE_LOGS)
                        SCRIPT_DEVICES[script_device]["THREAD"].start()
                    except:
                        logging.error("Could not add serial device " + config["PATH"])
                        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "Could not add serial device " + config["PATH"]
                        SCRIPT_FAIL = True
                        break
                else:
                    # Check if the supported bands match
                    if (SCRIPT_DEVICES[script_device]["2.4GHZ_REQ"] and not config["CLI_ATTACH_2.4GHZ"]) or \
                        (SCRIPT_DEVICES[script_device]["SUBGHZ_REQ"] and not config["CLI_ATTACH_SUBGHZ"]): continue
                    # Try creating an SSH device
                    try:
                        proc = ssh_dev(config["IP"], config["PORT"], config["USERNAME"], config["PASS"], config["SSHKEY_PATH"])
                        SCRIPT_DEVICES[script_device]["THREAD"] = dev_thread(proc, script_device, config["SUCCESS_STRING"], config["FAIL_STRING"], \
                                                                              test_log_write, MORE_DEVICE_LOGS)
                        SCRIPT_DEVICES[script_device]["THREAD"].start()
                    except:
                        logging.error("Could not add SSH device " + config["IP"])
                        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "Could not add SSH device " + config["IP"]
                        SCRIPT_FAIL = True
                        break
                # Assign ID to device config
                config["ID"] = script_device
                break

        if not SCRIPT_DEVICES[script_device]["THREAD"]:
            logging.error("device ID %s could not be assigned to a hardware device with compatible ROLE (%s)" % \
                          (script_device, SCRIPT_DEVICES[script_device]["ROLE"]))
            SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = script_device + " could not be assigned to a hardware device"
            SCRIPT_FAIL = True
            break

def destroy_radios():
    for config in DEVICE_CFGS:
        if not config["ID"]: continue
        destroy_device(config["ID"])
        SCRIPT_DEVICES[config["ID"]]["THREAD"].close()
        SCRIPT_DEVICES[config["ID"]]["THREAD"].join()
        SCRIPT_DEVICES[config["ID"]]["THREAD"] = None
        config["ID"] = ""

def line_replacements(line):
    newline = line
    dev = ""
    var = ""
    for str in line.split(" "):
        if len(str) > 0 and "{" in str and "}" in str:
            string_list = [char for char in str]
            new_str = str[string_list.index("{"):string_list.index("}") + 1]
            if new_str.find(":") != -1:
                dev, var = new_str.strip("{}").split(":")
            else:
                var = new_str.strip("{}")
            if SCRIPT_DEVICES and var == "NWKADDR":
                if dev in SCRIPT_DEVICES and SCRIPT_DEVICES[dev]["NWKADDR"]:
                    newline = line.replace(new_str, SCRIPT_DEVICES[dev]["NWKADDR"])
                else:
                    return newline
            elif SCRIPT_DEVICES and var == "EXTADDR":
                if dev in SCRIPT_DEVICES and SCRIPT_DEVICES[dev]["EXTADDR"]:
                    newline = line.replace(new_str, SCRIPT_DEVICES[dev]["EXTADDR"])
                else:
                    return newline
            elif CHANNEL_DICT is not None and var.startswith("CHANNEL"):
                if var == "CHANNEL_PAGE":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_PAGE"])
                elif var == "CHANNEL_MASK":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_MASK"])
                elif var == "CHANNEL_BDB_PRI_MASK":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_BDB_PRI_MASK"])
                elif var == "CHANNEL_BDB_SEC_MASK":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_BDB_SEC_MASK"])
                elif var == "CHANNEL_MASK_WITH_PAGE":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_MASK_WITH_PAGE"])
                elif var == "CHANNEL_MASK":
                    newline = line.replace(new_str, CHANNEL_DICT["CHANNEL_MASK"])
                elif (var == "CHANNEL") or var.startswith("CHANNEL+") or var.startswith("CHANNEL-"):
                    mask = int(CHANNEL_DICT["CHANNEL_MASK"], 16)
                    # SAL - do we know the channel the device is currently operating on?
                    # If yes, use that.
                    # If no, pick first channel from CHANNEL_MASK

                    for channel in range(27):
                        check = 1 << channel
                        if (check & mask) != 0:
                            break
                    else:
                        return None

                    if var.startswith("CHANNEL+"):
                        add_val = var.split("CHANNEL+")[1]
                        add_val = int(add_val)
                        channel += add_val
                        if channel > 27:
                            return None

                    elif var.startswith("CHANNEL-"):
                        sub_val = var.split("CHANNEL-")[1]
                        sub_val = int(sub_val)
                        channel -= sub_val
                        if channel < 0:
                            return None

                    newline = line.replace(new_str, "%d" % (channel))

            # zbcli-specific: Specify unique name for persistence file
            elif COND_ZBCLI_FEATURES and var == "PERSIST_PATH":
                rp_val = "%s_%s.persist" % (CURRENT_SCRIPT, ACTIVE_DEVICE)
                newline = line.replace(new_str, rp_val)
            elif COND_ZBCLI_FEATURES and var == "LINKKEY_PATH":
                rp_val = "%s_%s.linkkeys" % (CURRENT_SCRIPT, ACTIVE_DEVICE)
                newline = line.replace(new_str, rp_val)
            elif COND_ZBCLI_FEATURES and var == "OTA_SAVE_PATH":
                rp_val = "%s_%s.ota" % (CURRENT_SCRIPT, ACTIVE_DEVICE)
                newline = line.replace(new_str, rp_val)
            elif dev == "VAR" and var in DYNAMIC_REPLACEMENTS:
                newline = line.replace(new_str, DYNAMIC_REPLACEMENTS[var])
            elif var in USER_REPLACEMENTS:
                newline = line.replace(new_str, USER_REPLACEMENTS[var])
            else:
                return newline
            if len(var) > 1:
                return line_replacements(newline)
    return newline

def get_status_output(cmd):
    p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    return p.returncode, out.decode('UTF-8'), err.decode('UTF-8')

# Save current frame number from capture file
def update_frame_number(line_index=0):
    global TSHARK_FRAMENUM
    global TSHARK_FRAMENUM_BACKUP

    if COND_NO_CAPTURE or COND_NO_CHECKS:
        return

    if line_index != 0:
        search_index = line_index
        while search_index < len(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS]) - 1:
            search_index += 1
            line = line_replacements(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS][search_index][COMMAND_LIST_CMD])
            parameters = line.split(" ")
            if line[0] != "#" or parameters[0] == SCRIPT_ESCAPE_TSHARK_MARK_COMMAND:
                return
            elif parameters[0] == SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND:
                break
        # If upcoming command is device command and that device is disabled, tell user to execute all commands up to this point.
        prev_line = line_replacements(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS][line_index][COMMAND_LIST_CMD])
        if SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"] and not COND_FAST and prev_line.split(" ")[0] not in SCRIPT_ESCAPE_COMMANDS:
            logging.debug_highlight("Frame number filter must be incremented to ensure only relevant packets are validated" + \
                " by tshark. Please execute %s device commands up to this point, then press Enter to continue" % ACTIVE_DEVICE)
            input()

    cmd = "tshark -r " + SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".pcapng"
    output = get_status_output(cmd)[1]
    if output != "":
        if TSHARK_FRAMENUM > TSHARK_FRAMENUM_BACKUP:
            # Backup frame number is increased after each active device block, to prevent
            # issues where backup is too far behind. Be sure to only set backup to previous
            # frame number is previous frame number is ahead of backup.
            TSHARK_FRAMENUM_BACKUP = TSHARK_FRAMENUM
            #logging.info_highlight("Backup frame number updated %d" % TSHARK_FRAMENUM_BACKUP)
        TSHARK_FRAMENUM = len(output.strip().split("\n"))
        logging.info_highlight("Frame number filter incremented (frame.number > %s)" % TSHARK_FRAMENUM)
    elif TSHARK_MAIN:
        logging.info_highlight("No output from tshark, frame number filter was not incremented (frame.number > %s)" % TSHARK_FRAMENUM)

def tshark_save(search_params):
    global SCRIPT_FAIL

    if COND_NO_CAPTURE or COND_NO_CHECKS:
        return

    if len(search_params) != 5:
        logging.error("tshark-save: incorrect syntax")
        SCRIPT_FAIL = True
        return

    filter = search_params[TSHARK_SEARCH_FILTER]
    timeout = search_params[TSHARK_SEARCH_TIMEOUT]
    field = search_params[TSHARK_SEARCH_FIELD]
    var = search_params[TSHARK_SEARCH_VARIABLE]

    cmd = "tshark -2 -r " + SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".pcapng -Y \"" + filter + \
        " && frame.number > " + str(TSHARK_FRAMENUM) + \
        "\" -T fields -e " + field
    for key in ZIGBEE_KEYS:
        cmd += " -o 'uat:zigbee_pc_keys:\"%s\",\"Normal\",\"foo\"'" % key
    for key_params in ZD_KEYS:
        cmd += " -o 'uat:zigbee_direct_pc_keys:\"%s\",\"%s\",\"%s\",\"%s\"'" % (key_params[0], key_params[1], key_params[2], "foo")

    time_now = time.time()
    while time.time() < (time_now + float (timeout)):
        (status, output, error) = get_status_output(cmd)
        packet_list = output.splitlines()
        output_list_len = len(packet_list)

        if status:
            logging.error("tshark-save: tshark failed to start (cmd = " + cmd + ")")
            logging.error("ERR: " + error)
            SCRIPT_FAIL = True
            return
        else:
            # Assuming count_operator will always be >0
            if output_list_len > 0:
                rplc = ""
                for line in packet_list:
                    rplc = line.strip()
                    logging.info_highlight("  %s" % rplc)
                DYNAMIC_REPLACEMENTS[var] = rplc
                logging.info_highlight("Replacement added: " + var + "=" + DYNAMIC_REPLACEMENTS[var])
                return
            time.sleep(1)

    if COND_FAST:
        logging.debug_highlight("tshark-save failed, trying lower frame number filter...")
        cmd = cmd.replace("frame.number > " + str(TSHARK_FRAMENUM), "frame.number > " + str(TSHARK_FRAMENUM_BACKUP))
        (status, output) = get_status_output(cmd)
        packet_list = output.splitlines()
        output_list_len = len(packet_list)
        if not status:
            if output_list_len > 0:
                logging.warning("WARNING: tshark-save passed with backup frame number filter (frame.number > " + \
                    str(TSHARK_FRAMENUM_BACKUP) + "). Check capture file to validate success.")
                rplc = ""
                for line in packet_list:
                    rplc = line.strip()
                    logging.info_highlight("  %s" % rplc)
                DYNAMIC_REPLACEMENTS[var] = rplc
                logging.info_highlight("Replacement added: " + var + "=" + DYNAMIC_REPLACEMENTS[var])
                return
    logging.error("tshark-save: FAILURE")
    SCRIPT_FAIL = True

# Find packets that meet search params after saved frame number
def eval_pcap(search_params):
    global SCRIPT_FAIL

    if COND_NO_CAPTURE or COND_NO_CHECKS:
        return

    if len(search_params) != 3:
        logging.error("tshark-search: incorrect syntax")
        SCRIPT_FAIL = True
        return

    filter = search_params[TSHARK_SEARCH_FILTER]
    timeout = search_params[TSHARK_SEARCH_TIMEOUT]
    count_operator = search_params[TSHARK_SEARCH_NUM][0]
    count = int(search_params[TSHARK_SEARCH_NUM][1:])
    cmd = "tshark -2 -r " + SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".pcapng -Y '" + filter + \
        " && frame.number > " + str(TSHARK_FRAMENUM) + "'"
    for key in ZIGBEE_KEYS:
        cmd += " -o 'uat:zigbee_pc_keys:\"%s\",\"Normal\",\"foo\"'" % key
    for key_params in ZD_KEYS:
        cmd += " -o 'uat:zigbee_direct_pc_keys:\"%s\",\"%s\",\"%s\",\"%s\"'" % (key_params[0], key_params[1], key_params[2], "foo")

    time_now = time.time()
    while time.time() < (time_now + float (timeout)):
        if COND_FAST and sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
            line = input()
            logging.warning("Skipping tshark check.")
            SCRIPT_FAIL = False
            return
        (status, output, error) = get_status_output(cmd)
        packet_list = output.splitlines()
        output_list_len = len(packet_list)

        if status:
            if not COND_FAST:
                logging.error("tshark-search: tshark failed to start (cmd = " + cmd + ")")
                logging.error("ERR: " + error)
            else:
                logging.error("tshark failed to start, retrying in 5 seconds... (press Enter to skip)")
                logging.error("(cmd = " + cmd + ")")
                logging.error("ERR: " + error)
                time.sleep(5)
            SCRIPT_FAIL = True
            return
        else:
            if (count_operator == ">" and output_list_len > count):
                for line in packet_list:
                    logging.info_highlight(line)
                SCRIPT_FAIL = False
                return
            time.sleep(1)

    # Wait for timeout if there is an upper limit on packets expected
    if (count_operator == "=" and output_list_len == count) or \
        (count_operator == "<" and output_list_len < count):
        for line in packet_list:
            logging.info_highlight(line)
        SCRIPT_FAIL = False
        return

    # Timeout
    for line in packet_list:
        logging.error(line)
    if not COND_FAST:
        logging.error("tshark-search: FAILURE")
    elif count_operator == ">" or (count_operator == "=" and count > 0):
        if TSHARK_SEARCH_FAST_TEXT: logging.debug_highlight("tshark-search failed, trying lower frame number filter...")
        cmd = cmd.replace("frame.number > " + str(TSHARK_FRAMENUM), "frame.number > " + str(TSHARK_FRAMENUM_BACKUP))
        (status, output, error) = get_status_output(cmd)
        packet_list = output.splitlines()
        output_list_len = len(packet_list)
        if not status:
            if (count_operator == ">" and output_list_len > count) or \
                (count_operator == "=" and output_list_len == count):
                logging.warning("WARNING: tshark-search passed with backup frame number filter (frame.number > " + \
                    str(TSHARK_FRAMENUM_BACKUP) + "). Check capture file to validate success.")
                for line in packet_list:
                    logging.info_highlight(line)
                SCRIPT_FAIL = False
                return
    SCRIPT_FAIL = True

def get_cmd_timeout(line):
    timeout = TIMEOUT_COMMAND_DEFAULT
    for command in TIMEOUT_OVERRIDE:
        if line.find(command) != -1:
            timeout = int(TIMEOUT_OVERRIDE[command])
    if COND_ZBCLI_FEATURES and "wait" in line:
        try:
            parameters = line.split(" ")
            value_type = parameters[1].strip("-")
            value = int(parameters[2])
        except:
            logging.warning("WARNING: failed to set timeout based on zbcli wait command")
            return timeout
        if "sec" in value_type:
            timeout = value + 5
        elif "min" in value_type:
            timeout = (60 * value) + 5
    return timeout

# Send command to device. Return True on success, False on failure
def send_to_device(device_prompt, command):
    timeout = get_cmd_timeout(command)
    status = SCRIPT_DEVICES[device_prompt]["THREAD"].run_command(command, timeout=timeout)
    if not status:
        logging.error(command + ": FAILURE")
        return False
    else:
        return True

def initialize_device(prompt):
    global SCRIPT_FAIL

    # If no devices module is configured, do nothing
    if not DEVICES_MODULE:
        return

    # If device is initialized, do nothing
    if prompt in INITIALIZED_DEVICES:
        return

    # If device is disabled, pause to let user connect device by other means
    if SCRIPT_DEVICES[prompt]["DISABLED"] and not COND_FAST:
        logging.debug_highlight("Please initialize " + prompt + ", then press Enter to continue")
        input()
        INITIALIZED_DEVICES.append(prompt)
        return

    logging.debug_highlight("Initialize device: " + prompt)
    # Call device_init for active device
    for config in DEVICE_CFGS:
        if config["ID"] == prompt:
            status = False
            if config["TYPE"] == "SERIAL":
                try:
                    status = DEVICES_MODULE.serial_device_init(SCRIPT_DEVICES[prompt], config)
                except:
                    logging.error("Failed to execute serial_device_init(). Please check your devices file %s" % DEVICES_MODULE_PATH)

            elif config["TYPE"] == "SSH":
                try:
                    status = DEVICES_MODULE.ssh_device_init(SCRIPT_DEVICES[prompt], config)
                except:
                    logging.error("Failed to execute ssh_device_init(). Please check your devices file %s" % DEVICES_MODULE_PATH)

            if not status:
                logging.error("device init: FAILURE")
                SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "Device initialization: " + prompt
                SCRIPT_FAIL = True
                return
            INITIALIZED_DEVICES.append(prompt)

def destroy_device(prompt):
    global SCRIPT_FAIL

    # If no devices module is configured, do nothing
    if not DEVICES_MODULE:
        return

    # If device is not initialized, do nothing
    if prompt not in INITIALIZED_DEVICES:
        return

    # If device is disabled, pause to let user disconnect device
    if SCRIPT_DEVICES[prompt]["DISABLED"] and not COND_FAST:
        logging.debug_highlight("Please perform teardown for " + prompt + ", then press Enter to continue")
        input()
        INITIALIZED_DEVICES.remove(prompt)
        return

    # Call DEVICES_MODULE.device_teardown for active device
    logging.debug_highlight("Teardown device: " + prompt)
    for config in DEVICE_CFGS:
        if config["ID"] == prompt:
            status = False
            if config["TYPE"] == "SERIAL":
                try:
                    status = DEVICES_MODULE.serial_device_teardown(SCRIPT_DEVICES[prompt], config)
                except:
                    logging.error("Failed to execute serial_device_teardown(). Please check your devices file %s" % DEVICES_MODULE_PATH)

            elif config["TYPE"] == "SSH":
                try:
                    status = DEVICES_MODULE.ssh_device_teardown(SCRIPT_DEVICES[prompt], config)
                except:
                    logging.error("Failed to execute ssh_device_teardown(). Please check your devices file %s" % DEVICES_MODULE_PATH)

            if not status:
                logging.error("device teardown: FAILURE")
                SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "Device teardown: " + prompt
                SCRIPT_FAIL = True
                return
            INITIALIZED_DEVICES.remove(prompt)


def handle_hw_command(parameters):
    if parameters[1] == HW_COMMAND_RESET:
        destroy_device(ACTIVE_DEVICE)
        initialize_device(ACTIVE_DEVICE)

# Create file to open capture and inject zigbee keys
def add_capture_file_script():
    if not COND_NO_CAPTURE and TSHARK_MAIN and os.path.exists(SCRIPT_RESULTS_DIR) and (len(ZIGBEE_KEYS) or len(ZD_KEYS)):
        ws_cmd = "wireshark"
        for key in ZIGBEE_KEYS:
            ws_cmd += " -o 'uat:zigbee_pc_keys:\"%s\",\"Normal\",\"foo\"'" % key
        for key_params in ZD_KEYS:
            ws_cmd += " -o 'uat:zigbee_direct_pc_keys:\"%s\",\"%s\",\"%s\",\"%s\"'" % (key_params[0], key_params[1], key_params[2], "foo")
        if BLE_INTERFACE_LIST:
            ws_cmd += " -Y '(wpan || zbd || btle.advertising_address)'"
        elif ZB_INTERFACE_LIST:
            ws_cmd += " -Y 'wpan'"
        ws_cmd += " %s" % (CURRENT_SCRIPT + ".pcapng")

        wscmdscript_path = "%s/%s_keys.sh" % (SCRIPT_RESULTS_DIR, CURRENT_SCRIPT)
        templog = open(wscmdscript_path, "w")
        templog.write("#!/bin/sh\n\n")
        templog.write(ws_cmd)
        templog.write("\n")
        templog.close()
        cmd = "chmod 770 %s" % (wscmdscript_path)
        os.system(cmd)

# If a device is disabled, this script may not be able to obtain addresses or keys
# from stdout of the disabled device.
def disabled_device_controls():
    global COND_ADD_INFO
    data = ""
    keypress = ""
    text_prompt = "Press Enter to continue, a + Enter to input device addresses, k + Enter to input Zigbee key or d + Enter to disabled this menu"

    # Pause to give user time to let disabled device catch up
    logging.debug_highlight("Please execute %s commands up to this point. %s" % (ACTIVE_DEVICE, text_prompt))
    keypress = input()
    while keypress != "":
        if keypress == "a":
            logging.debug_highlight("Address Format: DEVICE_PROMPT, EUI, NWKADDR")
            logging.debug_highlight("Please input Device and Addresses then press enter:")
            data = input()
            if data:
                try:
                    prompt, extaddr, nwkaddr = data.split(", ")
                    SCRIPT_DEVICES[prompt]["EXTADDR"] = extaddr
                    SCRIPT_DEVICES[prompt]["NWKADDR"] = nwkaddr
                    logging.info_highlight("New extended address: " + prompt + "=" + extaddr)
                    logging.info_highlight("New network address: " + prompt + "=" + nwkaddr)
                except:
                    logging.error("Failed to add addresses, please check your input")
            else:
                logging.warning("Failed to add addresses, no input")

        elif keypress == "k":
            logging.debug_highlight("Zigbee key format: KEY")
            logging.debug_highlight("Zigbee Direct key format: ZDD_EUI, ZVD_EUI, SESSION_KEY")
            logging.debug_highlight("Please input key then press enter:")
            data = input()
            if "," in data:
                if not add_zd_key(data):
                    logging.error("Failed to add zigbee direct keys, please check your input")
            elif data:
                ZIGBEE_KEYS.append(data)
                logging.info_highlight("New zigbee key: " + data)
            else:
                logging.error("Failed to add keys, no input")

        elif keypress == "d":
            logging.debug_highlight("disabled add info menu")
            COND_ADD_INFO = False
            return

        else:
            logging.debug_highlight("Unrecognized keypress. Press Enter to continue, k + Enter to input Zigbee Key," \
                + "or a + Enter to input device addresses.")
            keypress = input()
            continue

        logging.debug_highlight(text_prompt)
        keypress = input()

def run_current_script():
    global ZIGBEE_KEYS
    global SCRIPT_FAIL
    global TSHARK_FRAMENUM
    global INITIALIZED_DEVICES
    global NWKADDR_THREAD
    global TEST_CASE_START_TIME
    global ACTIVE_DEVICE
    global TSHARK_SEARCH_FAST_TEXT
    global ZD_KEYS
    global TSHARK_FRAMENUM_BACKUP
    global SCRIPT_RESULTS_DIR
    global EXPECTED_DEV_ANNCE

    ACTIVE_DEVICE = ""
    group = 0
    ZIGBEE_KEYS = []
    ZD_KEYS = []
    INITIALIZED_DEVICES = []
    ZIGBEE_KEYS.extend(ZIGBEE_KEYS_DEFAULT)
    ZD_KEYS.extend(ZD_KEYS_DEFAULT)

    keypress = None
    prev_cmd = ""
    continue_cmds = True
    cmd_success = False
    end_loop = True

    if not COND_NO_CAPTURE and TSHARK_MAIN:
        NWKADDR_THREAD = nwkaddr_thread()
        NWKADDR_THREAD.start()
    if not SCRIPT_FAIL:
        logging.info("Starting test...")
        logging.info("-----------------------------------------------------------------------")
    TEST_CASE_START_TIME = datetime.now()

    for i, (lno, line) in enumerate(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS]):
        if SCRIPT_FAIL == True: break
        keypress = None
        line = line_replacements(line)
        if line[0:1] !="#":
            if group:
                group = group - 1
            else:
                update_frame_number(i)
        parameters = line.split(" ")
        if ACTIVE_DEVICE and SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"] and line[0:1] !="#":
            logging.info("[   Disabled   ] %s. %s" % (lno, line.strip()))
        else:
            logging.info("[" + str(get_tc_time_elapsed()) + "] %s. %s" % (lno, line.strip()))

        # Set active device
        if parameters[0] == SCRIPT_ESCAPE_DEVICE_COMMAND:
            if ACTIVE_DEVICE and COND_ADD_INFO and SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
                disabled_device_controls()

            ACTIVE_DEVICE = parameters[1]
            logging.debug_highlight("-----------------------------------------------------------------------")
            logging.debug_highlight("Set active device: " + ACTIVE_DEVICE)

            if DISABLED_TYPES:
                # If there are disabled devices, pause before commands continue
                continue_cmds = False
                # Make sure backup frame number doesn't fall too far behind in fast mode
                if COND_FAST and not SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
                    if not COND_NO_CAPTURE and not COND_NO_CHECKS:
                        cmd = "tshark -r " + SCRIPT_RESULTS_DIR + CURRENT_SCRIPT + ".pcapng"
                        output = get_status_output(cmd)[1]
                        if output != "":
                            TSHARK_FRAMENUM_BACKUP = len(output.strip().split("\n"))
                            #logging.info_highlight("Backup frame number updated %d" % TSHARK_FRAMENUM_BACKUP)

            if not SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
                # Initialize device if uninitialized
                initialize_device(ACTIVE_DEVICE)

        elif parameters[0] == SCRIPT_ESCAPE_SCRIPT_COMMAND:
            if len(parameters) < 2:
                logging.error("Incorrect script command format (#script: COMMAND ARGS)")
                SCRIPT_FAIL = True
            else:
                handle_script_command(parameters)

        elif parameters[0] == SCRIPT_ESCAPE_HW_COMMAND:
            handle_hw_command(parameters)

        elif parameters[0] == SCRIPT_ESCAPE_INIT_COND_COMMAND or \
            parameters[0] == SCRIPT_ESCAPE_TEST_PROC_COMMAND or \
            parameters[0] == SCRIPT_ESCAPE_EXP_OUT_COMMAND or \
            parameters[0] == SCRIPT_ESCAPE_USER_COMMAND:
            logging.debug_teststep(line)
            # Continue so that prev_cmd isn't updated
            continue

        elif parameters[0].startswith('#tshark') and not COND_NO_CAPTURE and not COND_NO_CHECKS:
            # Validate packets based on search criteria
            if parameters[0] == SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND or parameters[0] == SCRIPT_ESCAPE_TSHARK_SAVE_COMMAND:
                # Make sure disabled device is in sync
                if SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"] \
                    and prev_cmd.split(" ")[0] not in TSHARK_SEARCH_COMMANDS:
                    if COND_ADD_INFO:
                        disabled_device_controls()
                        line = line_replacements(line)
                    if not COND_FAST:
                        logging.debug_highlight("Execute %s command '%s' then press Enter to execute tshark-search" % (ACTIVE_DEVICE, prev_cmd))
                        input()

                arg_list = line.split(parameters[0] + " ")[1].split(",")
                arg_list = [entry.strip() for entry in arg_list]
                if parameters[0] == SCRIPT_ESCAPE_TSHARK_SEARCH_COMMAND:
                    if COND_FAST: TSHARK_SEARCH_FAST_TEXT = True
                    eval_pcap(arg_list)
                    while COND_FAST and SCRIPT_FAIL:
                        if TSHARK_SEARCH_FAST_TEXT:
                            # Only display this message once
                            logging.debug_highlight("tshark-search failed, looping until success... (press Enter to skip)")
                            TSHARK_SEARCH_FAST_TEXT = False
                        eval_pcap(arg_list)
                else:
                    tshark_save(arg_list)

            # Update frame number and stop automatic updates while a group of commands execute
            elif parameters[0] == SCRIPT_ESCAPE_TSHARK_MARK_COMMAND:
                group = 0
                if len(parameters) == 2: group = int(parameters[1].split("group=")[1])
                update_frame_number()

        # Handle device commands
        elif line[0:1] !="#" and not SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
            # In disabled device tests, user controls the pace of commands (continue_cmds always true during regular tests)
            if not continue_cmds:
                while keypress != "" and keypress != "c":
                    logging.debug_highlight("Press 'c' + Enter to continue, Enter to run next command," + \
                        " 'r' + Enter to rerun the last command, or 'm' + Enter to manually input a command: ")
                    keypress = input()
                    if keypress == "c":
                        continue_cmds = True
                    elif keypress == "r":
                        if prev_cmd[0:1] !="#":
                            logging.info("[" + str(get_tc_time_elapsed()) + "] %s (manual retry)" % prev_cmd)
                            send_to_device(ACTIVE_DEVICE, prev_cmd)
                        else:
                            logging.debug_highlight("Can only retry device commands. Previous: %s\n" % prev_cmd)
                    elif keypress == "m":
                        logging.debug_highlight("Enter a manual command: ")
                        input_cmd = input()
                        if input_cmd != "":
                            logging.info("[" + str(get_tc_time_elapsed()) + "] %s (manual command)" % input_cmd)
                            send_to_device(ACTIVE_DEVICE, input_cmd)

            # Send current command to active device for execution
            cmd_success = send_to_device(ACTIVE_DEVICE, line)

            if not cmd_success:
                if not DISABLED_TYPES:
                    # In fully automated tests, retry command upon failure
                    for i in range(RETRY_COUNT):
                        time.sleep(5)
                        logging.info("%s (retry %d)" % (line, 1+i))
                        cmd_success = send_to_device(ACTIVE_DEVICE, line)
                        if cmd_success:
                            break
                else:
                    # In disabled device tests, allow user to decide what to do upon command failure
                    logging.debug_highlight("Command failed, press Enter to quit/continue, 'r' + Enter to rerun the failed command," + \
                        " or 'm' + Enter to manually input a command: ")
                    keypress = input()
                    while keypress != "":
                        if keypress == "r":
                            logging.info("[" + str(get_tc_time_elapsed()) + "] %s (manual retry)" % line)
                            cmd_success = send_to_device(ACTIVE_DEVICE, line)
                        elif keypress == "m":
                            logging.debug_highlight("Enter a manual command:\n")
                            input_cmd = input()
                            if input_cmd != "":
                                logging.info("[" + str(get_tc_time_elapsed()) + "] %s (manual command)" % input_cmd)
                                cmd_success = send_to_device(ACTIVE_DEVICE, input_cmd)
                        keypress = input()

            if not cmd_success:
                SCRIPT_FAIL = True
            if COND_ZBCLI_FEATURES:
                # zbcli-specific: print addressing info after particular commands
                if (line.startswith("init ") or line.startswith("startup_form") or line.startswith("startup_join") \
                    or line.startswith("zvd_app tunn_join") or line.startswith("status")) and SCRIPT_FAIL == False:
                    if not send_to_device(ACTIVE_DEVICE, "status --addr"):
                        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = "status --addr"
                        SCRIPT_FAIL = True
                # zbcli-specific: make sure keys are stored after particular commands
                if (line.startswith("zcl_ke_negot") or line.startswith("aps_request_key")) and SCRIPT_FAIL == False:
                    # Takes a couple seconds for key to get stored properly
                    time.sleep(2)
                # zbcli-specific: tcso backup
                if line.startswith("aps_link_key load") and SCRIPT_FAIL == True:
                    if device_restore_TCSO_keys(ACTIVE_DEVICE):
                        logging.info_highlight("aps_link_key load: succeeded using saved backup key info.")
                        SCRIPT_FAIL = False

        # Store previous command
        prev_cmd = line

        if EXPECTED_DEV_ANNCE_CMDS:
            if ACTIVE_DEVICE and SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
                # Peek at script commands that would be sent to disabled device
                for cmd in EXPECTED_DEV_ANNCE_CMDS:
                    if cmd in line:
                        # Expecting a device to join network, so wait for matching addressing info
                        # This procedure is required to update addressing info if joiner doesn't have known extended address
                        EXPECTED_DEV_ANNCE = ACTIVE_DEVICE
                        logging.debug_highlight("Expecting a device announcement for disabled device: " + EXPECTED_DEV_ANNCE)
                        if COND_FAST:
                            DEV_ANNCE_EVENT.wait(get_cmd_timeout("startup_join"))
                        else:
                            logging.debug_highlight("Press enter to continue if the device announcement is parsed or to skip pause")
                            input()
                        break

    if not SCRIPT_FAIL and SCRIPT_DEVICES[ACTIVE_DEVICE]["DISABLED"]:
        if COND_ADD_INFO:
            disabled_device_controls()

    if DISABLED_TYPES:
        while end_loop:
            logging.debug_highlight("Reached end of script. Input 'q' to conclude test, " + \
                "'m' to input manual command, or 's' to switch active device:")
            keypress = input()
            while keypress != "":
                if keypress == "q":
                    end_loop = False
                    break
                elif keypress == "m":
                    if ACTIVE_DEVICE in DISABLED_TYPES:
                        logging.debug_highlight("Active device %s is disabled." % ACTIVE_DEVICE)
                        break
                    logging.debug_highlight("Enter a manual command to send to " + ACTIVE_DEVICE + ":")
                    input_cmd = input()
                    if input_cmd != "":
                        send_to_device(ACTIVE_DEVICE, input_cmd)
                        break
                elif keypress == "s":
                    logging.debug_highlight(str(list(SCRIPT_DEVICES.keys())))
                    logging.debug_highlight("Enter a device role to switch to (Active: " + ACTIVE_DEVICE + "):")
                    input_dev = input()
                    if input_dev in SCRIPT_DEVICES:
                        ACTIVE_DEVICE = input_dev
                    else:
                        logging.debug_highlight("Invalid device")
                    break
                else:
                    logging.debug_highlight("Invalid input")
                    break

    # Conclude current test
    if NWKADDR_THREAD:
        NWKADDR_THREAD.kill = True
        NWKADDR_THREAD.join()
    if CBKE_CERT_LIST:
        # Reset in-use flag for CBKE
        for item in CBKE_CERT_LIST:
            item[2] = False
    TSHARK_FRAMENUM = 0
    logging.info("-----------------------------------------------------------------------")
    logging.info(CURRENT_SCRIPT + (" FAIL" if SCRIPT_FAIL else " PASS"))
    DEV_LOG.write("\n\n>>>>>>>>>>>>>>>\nFINISHED SCRIPT: \"" + CURRENT_SCRIPT + \
        ("\" FAIL\n<<<<<<<<<<<<<<<\n\n" if SCRIPT_FAIL else "\" PASS\n<<<<<<<<<<<<<<<\n\n"))
    logging.info("DURATION: " + str(get_tc_time_elapsed()))
    result_append = "_FAIL"
    if SCRIPT_FAIL == False:
        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_PASS] = True
        result_append = "_PASS"
    elif prev_cmd and not SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL]:
        SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CAUSE_OF_FAIL] = prev_cmd

    os.rename(SCRIPT_RESULTS_DIR, SCRIPT_RESULTS_DIR.strip("/") + result_append)
    SCRIPT_RESULTS_DIR = SCRIPT_RESULTS_DIR.strip("/") + result_append
    SCRIPT_FAIL = False

def main():
    global CURRENT_SCRIPT
    global SCRIPT_FAIL

    # Setup
    parse_arguments()
    init_test_output()
    signal.signal(signal.SIGINT, signal_handler)
    read_config()
    init_test_scripts()
    print_header()

    # Main testing loop
    for script in SCRIPT_LIST:
        SCRIPT_DEVICES.clear()
        USER_REPLACEMENTS.clear()
        DYNAMIC_REPLACEMENTS.clear()
        CURRENT_SCRIPT = script
        USER_REPLACEMENTS.update(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_REPLACEMENTS])
        if not append_cmd_list(SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_CMDS], SCRIPT_LIST[CURRENT_SCRIPT][SCRIPT_DICT_PATH]):
            SCRIPT_FAIL = True
        init_script_output()
        init_sniffers()
        add_device_processes()
        run_current_script()
        add_capture_file_script()
        cleanup()

    # Conclude testing
    if DEV_LOG: DEV_LOG.close()
    close_active_logger_file()
    if len(SCRIPT_LIST) > 0:
        pass_cnt = 0
        with open(OUTPUT_DIR + 'summary.csv', 'w') as summary_csv:
            field_names = ['test case', 'result', 'cause of failure']
            csv_writer = csv.DictWriter(summary_csv, fieldnames=field_names)
            csv_writer.writeheader()
            logging.info("\n#----------------------------------------------------------------------")
            logging.info("Results:")
            for script in SCRIPT_LIST:
                if SCRIPT_LIST[script][SCRIPT_DICT_PASS]:
                    result_string = "PASS"
                    pass_cnt += 1
                else:
                    result_string = "FAIL"
                logging.info(script + " - " + result_string)
                csv_writer.writerow({'test case': script, 'result': result_string, 'cause of failure': SCRIPT_LIST[script][SCRIPT_DICT_CAUSE_OF_FAIL]})

        result_append = ""
        if len(SCRIPT_LIST) == pass_cnt:
            result_append = "_PASS"
        elif not pass_cnt:
            result_append += "_FAIL"
        if result_append:
            # Add PASS or FAIL to result directory if all tests pass/fail
            os.rename(OUTPUT_DIR, OUTPUT_DIR.strip("/") + result_append)
        logging.info(str(pass_cnt) + "/" + str(len(SCRIPT_LIST)) + " PASS (" + str(100*float(pass_cnt)/float(len(SCRIPT_LIST))) + "%)")

if __name__ == "__main__":
    main()
