# Copyright [2020 - 2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import serial
import paramiko
import time
import threading
import logging

logging.getLogger("paramiko").setLevel(logging.WARNING)

# Serial Device Class 
# Devices that execute commands in application running on firmware
class serial_dev:
    ser = None
    last_output = ""

    def __init__(self, port):
        self.port = port
        self.ser = serial.Serial(port=port, baudrate=115200, timeout=5)

    def reset(self):
        self.ser.close()
        self.ser = serial.Serial(port=self.port, baudrate=115200, timeout=5)

    def run_command(self, cmd):
        self.ser.write((cmd + "\r").encode())

    def read(self):
        output = ""
        try:
            read = self.ser.read(self.ser.inWaiting()).decode('utf-8')
            if read: output += read
            self.ser.flushOutput()
        except:
            pass
        return output

    def close(self):
        if self.ser != None:
            self.ser.close()

# SSH Device Class
# For running an application on an OS (debian or linux) which can connect to and communicate with a device
class ssh_dev:
    shell = None
    client = None
    last_output = ""
    host_address = ""
    port = 22
    username = None
    password = None
    key_path = None

    def __init__(self, host_address, port, username, password, key_path):
        self.host_address = host_address
        if port: self.port = port
        if username: self.username = username
        if password: self.password = password
        if key_path: self.key_path = key_path
        self.client = paramiko.client.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
        self.client.connect(self.host_address, port=self.port, username=self.username, password=self.password, key_filename=self.key_path)
        self.shell = self.client.invoke_shell()

    def reset(self):
        self.client.close()
        self.client = paramiko.client.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
        self.client.connect(self.host_address, port=self.port, username=self.username, password=self.password, key_filename=self.key_path)
        self.shell = self.client.invoke_shell()

    def run_command(self, cmd):
        self.shell.send(cmd + "\n")
    
    def read(self):
        output = ""
        if self.shell.recv_ready():
            while self.shell.recv_ready():
                    output += self.shell.recv(1024).decode("utf-8")
        return output
    
    def close(self):
        if self.client != None:
            self.client.close()

class dev_thread(threading.Thread):
    def __init__(self, device, prompt, success_str, fail_str, log_write, ignore_print_log):
        threading.Thread.__init__(self)
        self.device = device
        self.log_write = log_write
        self.success_str = success_str
        self.fail_str = fail_str
        self.kill = False
        self.debug = ""
        self.saved_output = ""
        self.role = prompt
        self.new_cmd = False
        self.buffer = ""
        self.ignore_print_log = ignore_print_log

    def reset(self):
        self.device.reset()
    
    def get_last_output(self):
        return self.saved_output

    def buffer_read(self, read):
        output = ""
        for char in read:
            self.buffer += char
            if char == '\n':
                output += self.buffer
                self.buffer = ""
        return output

    def run_command(self, cmd, check_status=True, print_log=True, timeout=0):
        self.saved_output = ""
        self.new_cmd = True
        time_now = time.time()

        self.device.run_command(cmd)
        if not check_status or not self.success_str:
            time.sleep(0.1)
            self.debug = ""
            return True
        while time.time() < (time_now + float(timeout)):
            if self.debug:
                if print_log and not self.ignore_print_log:
                    for line in self.debug.splitlines():
                        if line.strip(): logging.debug("  " + line)
                self.debug = ""
                if self.success_str and (self.saved_output.find(self.success_str) != -1):
                    return True
                elif self.fail_str and (self.saved_output.find(self.fail_str) != -1):
                    return False
        logging.error("%s timeout waiting for success string = %s" % (cmd, self.success_str))
        return False

    def close(self):
        self.kill = True
        if self.device: self.device.close()

    def run(self):
        while self.kill == False:
            output = self.buffer_read(self.device.read())
            if output:
                self.debug += output
                self.saved_output += output
                if self.new_cmd == True: 
                    self.new_cmd = False
                    self.log_write("\r\n")
                msg = "\n".join([self.role + ": " + line for line in output.splitlines() if line])
                self.log_write(msg)
            if not output:
                time.sleep(0.001)

        logging.warning(self.role + " thread killed")