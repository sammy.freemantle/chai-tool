# Copyright [2022-2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import time
import logging
import subprocess

DEFAULT_TIMEOUT = 6

def serial_device_init(script_device, hw_device):
    # Enable printing of command status to stdout
    if not script_device["THREAD"].run_command("echo cmd-status-on", timeout=DEFAULT_TIMEOUT):
        return False

    # Print input commands to stdout
    if not script_device["THREAD"].run_command("echo on", timeout=DEFAULT_TIMEOUT):
        return False

    # Set table size
    if "DEVICE_DICT_SZ_NNT" in script_device:
        cmd = "init_tbl_size --nwk-nnt %d" % (script_device["DEVICE_DICT_SZ_NNT"])
        if "DEVICE_DICT_SZ_HEAP" in script_device:
            cmd += " --heap %d" % (script_device["DEVICE_DICT_SZ_HEAP"])
            cmd += " --nwk-route %d" % (script_device["DEVICE_DICT_SZ_ROUTE"])
            cmd += " --nwk-addr %d" % (script_device["DEVICE_DICT_SZ_ADDR"])
            cmd += " --aps-link %d" % (script_device["DEVICE_DICT_SZ_LINK"])
        if not script_device["THREAD"].run_command(cmd, timeout=DEFAULT_TIMEOUT):
            return False

    # Initialize device
    if not script_device["THREAD"].run_command("init " + script_device["EXTADDR"], timeout=DEFAULT_TIMEOUT):
        return False
    
    for i, cmd in script_device["CBKE_CERT_CMDS"]:
        if not script_device["THREAD"].run_command(cmd, timeout=DEFAULT_TIMEOUT):
            return False

    return True

def ssh_device_init(script_device, hw_device):
    # Start CLI application
    script_device["THREAD"].run_command(hw_device["CLI_START"], check_status=False)
    time.sleep(0.5)

    # Enable printing of command status to stdout
    if not script_device["THREAD"].run_command("echo cmd-status-on", timeout=DEFAULT_TIMEOUT):
        return False

    # Attach 2.4GHz radio to CLI
    if script_device["2.4GHZ_REQ"] or not script_device["SUBGHZ_REQ"]:
        if hw_device["CLI_ATTACH_2.4GHZ"]:
            if not script_device["THREAD"].run_command(hw_device["CLI_ATTACH_2.4GHZ"], timeout=DEFAULT_TIMEOUT):
                return False

    # Attach SubGHz radio to CLI
    if script_device["SUBGHZ_REQ"]:
        if hw_device["CLI_ATTACH_SUBGHZ"]:
            if not script_device["THREAD"].run_command(hw_device["CLI_ATTACH_SUBGHZ"], timeout=DEFAULT_TIMEOUT):
                return False

    # Set table size
    if "DEVICE_DICT_SZ_NNT" in script_device:
        cmd = "init_tbl_size --nwk-nnt %d" % (script_device["DEVICE_DICT_SZ_NNT"])
        if "DEVICE_DICT_SZ_HEAP" in script_device:
            cmd += " --heap %d" % (script_device["DEVICE_DICT_SZ_HEAP"])
            cmd += " --nwk-route %d" % (script_device["DEVICE_DICT_SZ_ROUTE"])
            cmd += " --nwk-addr %d" % (script_device["DEVICE_DICT_SZ_ADDR"])
            cmd += " --aps-link %d" % (script_device["DEVICE_DICT_SZ_LINK"])
        if not script_device["THREAD"].run_command(cmd, timeout=DEFAULT_TIMEOUT):
            return False

    # Initialize EUI
    if not script_device["THREAD"].run_command("init " + script_device["EXTADDR"], timeout=DEFAULT_TIMEOUT):
        return False

    for i, cmd in script_device["CBKE_CERT_CMDS"]:
        if not script_device["THREAD"].run_command(cmd, timeout=DEFAULT_TIMEOUT):
            return False


    return True

def serial_device_teardown(script_device, hw_device):
    # Teardown covered during initialization
    return True

def ssh_device_teardown(script_device, hw_device):
    # Destroy the network connection and exit CLI application
    if not script_device["THREAD"].run_command("destroy", print_log=False, timeout=DEFAULT_TIMEOUT):
        return False
    if not script_device["THREAD"].run_command("exit", print_log=False, timeout=DEFAULT_TIMEOUT):
        return False
    time.sleep(0.5)
    return True
