# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

# The parse_keys_and_addresses function takes in a line of output from a device
# and returns a tuple with a value matching the type of string returned and a
# string in a specific format understood by chai-tool

import re

# First argument returned in tuple
PARSED_ADDRESSING = 0
PARSED_ZIGBEE_KEY = 1
PARSED_ZIGBEE_DIRECT_KEY = 2
PARSED_TCSO_BACKUP = 3

# Format of Second Argument returned in tuple
# PARSED_ADDRESSING : EXTADDR, NWKADDR : 0xaaaaaaaaaaaaaaaa, 0xffff
# PARSED_ZIGBEE_KEY : ZDKEY : 0123456789abcdef0123456789abcdef
# PARSED_ZIGBEE_DIRECT_KEY : ZDD_EUI, ZVD_EUI, Session Key : aaaaaaaaaaaaaaaa, 0000000000000001, 0123456789abcdef0123456789abcdef

# EXEGIN: Finding keys in log files
# entry = (search_str, name, is_zigbee_direct)
KEY_FIND_LIST = []
KEY_FIND_LIST.append(("Network Key: ", "Network Key", False))
KEY_FIND_LIST.append(("Preconf Key: ", "Preconf Key", False))
KEY_FIND_LIST.append(("Distrib Key: ", "Distrib Key", False))
KEY_FIND_LIST.append(("Session Key: ", "ZD Session Key", True))

def parse_keys_and_addresses(msg):
    parsed_strings = []
    extaddr = ""
    nwkaddr = ""
    for line in msg.splitlines():
        find_idx = line.find("Security Key Update : ")
        if find_idx != -1:
            # E.g. "Security Key Update : KEY : DESC"
            key_list = line[find_idx:].split(" : ")
            if len(key_list) == 3:
                key_str = key_list[1].strip()
                desc_str = key_list[2].strip()
                # Don't add blank keys
                if key_str != "00000000000000000000000000000000":
                    parsed_strings.append((PARSED_ZIGBEE_KEY, key_str))

        else:
            for key_find in KEY_FIND_LIST:
                if line.find(key_find[0]) != -1:
                    key_str = line.split(key_find[0])[1].strip()

                    if key_find[2]:
                        parsed_strings.append((PARSED_ZIGBEE_DIRECT_KEY, key_str))
                    else:
                        parsed_strings.append((PARSED_ZIGBEE_KEY, key_str))
                    break
            else:
                KEY_DELIM = " Key: "
                if line.find(KEY_DELIM) != -1:
                    # Check for "status :   0x001cdaffff0031e2 Key:"
                    (key_name, key_str) = line.split(KEY_DELIM)
                    key_name = key_name.split()[-1].strip()
                    if key_name.startswith("0x"):
                        # Convert to 00:1c:da:ff:ff:00:31:e2 format
                        key_name = key_name[2:]
                        key_name = ':'.join(key_name[i:i+2] for i in range(0, len(key_name), 2))
                    else:
                        # key_name = "Zigbee Key (unknown)"
                        return parsed_strings

                    # Remove separators from key
                    key_str = key_str.strip()
                    parsed_strings.append((PARSED_ZIGBEE_KEY, key_str))
                elif line.find("Addressing: ") != -1:
                    addrs = line.split("Addressing: ")[1]
                    parsed_strings.append((PARSED_ADDRESSING, addrs))
                elif line.find("aps_link_key : keyPair") != -1 or \
                    line.find("aps_link_key : Saving link keys to file") or \
                    line.find("Ignoring --filename argument") != -1:
                    parsed_strings.append((PARSED_TCSO_BACKUP, line))
                else:
                    if "Extended Address:" in line:
                        extaddr = line.partition("Extended Address:")[2].strip().lower()
                    elif "Short Address:" in line:
                        nwkaddr = line.partition("Short Address:")[2].strip().lower()
                        if extaddr:
                            parsed_strings.append((PARSED_ADDRESSING, extaddr + ", " + nwkaddr))
                    else:
                        addresses = re.findall(r'.*nwk_handle_assoc \: Association accepted, sending response\ \(([0-9A-z]{6})\,\ ([0-9A-z]{18})\)[.|\n]*', line)
                        if len(addresses):
                            extaddr = addresses[0][1]
                            nwkaddr = addresses[0][0]
                            parsed_strings.append((PARSED_ADDRESSING, extaddr + ", " + nwkaddr))
                        else:
                            addresses = re.findall(r'.*ZbZdoHandleDeviceAnnce : Received Device_Annce for\ ([0-9A-z]{18})\ \(([0-9A-z]{6})\)[.|\n]*', line)
                            if len(addresses):
                                extaddr = addresses[0][0]
                                nwkaddr = addresses[0][1]
                                parsed_strings.append((PARSED_ADDRESSING, extaddr + ", " + nwkaddr))
                            else:
                                addresses = re.findall(r'.*nwk_addr_map_entry_update : New address mapping \(extAddr =\ ([0-9A-z]{18})\,\ nwkAddr = ([0-9A-z]{6})[.|\n]*', line)
                                if len(addresses):
                                    extaddr == addresses[0][0]
                                    nwkaddr = addresses[0][1]
                                    parsed_strings.append((PARSED_ADDRESSING, extaddr + ", " + nwkaddr))
    return parsed_strings