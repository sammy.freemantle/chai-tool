# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import logging

FILE_HANDLER = None
STDOUT_HANDLER = None
ROOT_HANDLER = None

def debug_teststep(message, *args, **kwargs):
    logging.log(logging.DEBUG_TESTSTEP, message, *args, **kwargs)
def debug_highlight(message, *args, **kwargs):
    logging.log(logging.DEBUG_HIGHLIGHT, message, *args, **kwargs)
def info_highlight(message, *args, **kwargs):
    logging.log(logging.INFO_HIGHLIGHT, message, *args, **kwargs)

logging.DEBUG_TESTSTEP = 12
logging.DEBUG_HIGHLIGHT = 14
logging.INFO_HIGHLIGHT = 16
logging.addLevelName(logging.DEBUG_TESTSTEP, "DEBUG_TESTSTEP")
logging.addLevelName(logging.DEBUG_HIGHLIGHT, "DEBUG_HIGHLIGHT")
logging.addLevelName(logging.INFO_HIGHLIGHT, "INFO_HIGHLIGHT")
logging.debug_teststep = debug_teststep
logging.debug_highlight = debug_highlight
logging.info_highlight = info_highlight

class color_formatter(logging.Formatter):
    format = "%(message)s"
    red = "\x1b[38;5;1m"
    orange = "\x1b[38;5;208m"
    dark_yellow = "\x1b[38;5;178m"
    yellow = "\x1b[38;5;11m"
    green = "\x1b[38;5;34m"
    blue = '\x1b[38;5;38m'
    indigo = '\x1b[38;5;62m'
    violet = '\x1b[38;5;141m'
    white = '\x1b[38;5;255m'
    grey = "\x1b[38;5;7m"
    dark_grey = "\x1b[38;5;242m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    FORMATS = {
        logging.DEBUG: reset + dark_grey + format + reset,
        logging.INFO: reset + format,
        logging.WARNING: reset + yellow + format + reset,
        logging.ERROR: reset + red + format + reset,
        logging.CRITICAL: reset + bold_red + format + reset,
        logging.DEBUG_TESTSTEP: reset + green + format + reset,
        logging.DEBUG_HIGHLIGHT: reset + dark_yellow + format + reset,
        logging.INFO_HIGHLIGHT: reset + blue + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

def init_logger():
    global ROOT_HANDLER
    global STDOUT_HANDLER

    STDOUT_HANDLER = logging.StreamHandler()
    STDOUT_HANDLER.setLevel(logging.DEBUG)
    STDOUT_HANDLER.setFormatter(color_formatter())
    ROOT_HANDLER = logging.getLogger()
    ROOT_HANDLER.setLevel(logging.DEBUG)
    ROOT_HANDLER.addHandler(STDOUT_HANDLER)

def start_new_logger_file(path):
    global FILE_HANDLER

    close_active_logger_file()
    FILE_HANDLER = logging.FileHandler(filename=path)
    FILE_HANDLER.setLevel(logging.INFO_HIGHLIGHT)
    FILE_HANDLER.setFormatter(logging.Formatter("%(message)s"))
    ROOT_HANDLER.addHandler(FILE_HANDLER)

def close_active_logger_file():
    if FILE_HANDLER:
        ROOT_HANDLER.removeHandler(FILE_HANDLER)