# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import time
import logging

# This file contains generic examples of some of the procedures that may be required to
# set up and teardown your test devices. These examples demostrate how to use the API of
# the script device process, some of the useful values stored in the script device and
# hardware device objects and also some example CLI commands you may need to send to your
# devices during every initialization / teardown. The init functions are called before
# sending any commands to an SSH/Serial device, and the teardown functions are called at
# the end of a test. The '#hw: reset' command can be used to call the teardown and init
# functions consecutively, for tests where a reset is necessary.

DEFAULT_TIMEOUT = 5

def serial_device_init(script_device, hw_device):
    # Reset board and serial connection
    script_device["THREAD"].reset()

    # Initialize device with saved extended address
    if not script_device["THREAD"].run_command("init " + script_device["EXTADDR"], timeout=DEFAULT_TIMEOUT):
        return False

    return True

def ssh_device_init(script_device, hw_device):
    if hw_device["TAG"] == "linux_ping_demo":
        # Do nothing for device from ping-demo
        return True

    # Start CLI application without checking for returned status string
    script_device["THREAD"].run_command(hw_device["CLI_START"], check_status=False)

    # Attach 2.4GHz radio to CLI if required
    if script_device["2.4GHZ_REQ"] or not script_device["SUBGHZ_REQ"]:
        if hw_device["CLI_ATTACH_2.4GHZ"]:
            if not script_device["THREAD"].run_command(hw_device["CLI_ATTACH_2.4GHZ"], timeout=DEFAULT_TIMEOUT):
                return False

    # Attach SubGHz radio to CLI if required
    if script_device["SUBGHZ_REQ"]:
        if hw_device["CLI_ATTACH_SUBGHZ"]:
            if not script_device["THREAD"].run_command(hw_device["CLI_ATTACH_SUBGHZ"], timeout=DEFAULT_TIMEOUT):
                return False

    # Initialize device with saved extended address
    if not script_device["THREAD"].run_command("init " + script_device["EXTADDR"], timeout=DEFAULT_TIMEOUT):
        return False

    return True

def serial_device_teardown(script_device, hw_device):
    # Reset CLI application
    if not script_device["THREAD"].run_command("board reset", timeout=DEFAULT_TIMEOUT):
        return False

    return True

def ssh_device_teardown(script_device, hw_device):
    if hw_device["TAG"] == "linux_ping_demo":
        # Do nothing for device from ping-demo
        return True

    # Destroy the network connection and exit CLI application
    if not script_device["THREAD"].run_command("destroy", print_log=False, timeout=DEFAULT_TIMEOUT):
        return False
    if not script_device["THREAD"].run_command("exit", print_log=False, timeout=DEFAULT_TIMEOUT):
        return False

    return True