# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

# The parse_keys_and_addresses function takes in each line of output from a device
# and returns a tuple with a supported string type value and a string of the
# specified type

# First argument returned in tuple
PARSED_ADDRESSING = 0
PARSED_ZIGBEE_KEY = 1
PARSED_ZIGBEE_DIRECT_KEY = 2

# Format of Second Argument returned in tuple
# PARSED_ADDRESSING : EXTADDR, NWKADDR : 0xaaaaaaaaaaaaaaaa, 0xffff
# PARSED_ZIGBEE_KEY : ZDKEY : 0123456789abcdef0123456789abcdef
# PARSED_ZIGBEE_DIRECT_KEY : ZDD_EUI, ZVD_EUI, Session Key : aaaaaaaaaaaaaaaa, 0000000000000001, 0123456789abcdef0123456789abcdef

# Identifiers for keys and addresses and their respective return types
KEY_FIND_LIST = []
KEY_FIND_LIST.append(("Addressing: ", PARSED_ADDRESSING))
KEY_FIND_LIST.append(("Network Key: ", PARSED_ZIGBEE_KEY))
KEY_FIND_LIST.append(("Preconf Key: ", PARSED_ZIGBEE_KEY))
KEY_FIND_LIST.append(("Distrib Key: ", PARSED_ZIGBEE_KEY))
KEY_FIND_LIST.append(("Session Key: ", PARSED_ZIGBEE_DIRECT_KEY))

def parse_keys_and_addresses(msg):
    parsed_strings = []
    # Check line for each identifier and extract value if it appears
    for line in msg.splitlines():
        for key_find in KEY_FIND_LIST:
            if line.find(key_find[0]) != -1:
                key_str = line.split(key_find[0])[1].strip()
                parsed_strings.append((key_find[1], key_str))
                break
    return parsed_strings