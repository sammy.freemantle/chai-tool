# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

import threading
import logging
import time
import subprocess
import fcntl
import select
import os

# Sniffer thread classes should use the configuration data to
# initialise the sniffer application and run it in the background.
# The user may want to add code for a check function to ensure the
# application starts successfully. chai-tool waits for the check
# function to return true before starting a test run

# This thread runs a zigbee sniffer application in the background
class sniffer_thread(threading.Thread):
    def __init__ (self, app_path, dev_path, channel, page, interface):
        threading.Thread.__init__(self)
        self.daemon = True
        self.app_path = app_path
        self.dev_path = dev_path
        self.channel = channel
        self.page = page
        self.interface = interface
        self.p = None
        self.kill = False
        self.running = False

    # chai-tool executes this function after the thread starts
    # to check if the application started succesfully before
    # continuing
    def check(self):
        end_time = time.time() + 5
        while time.time() < end_time:
            if not self.p:
                # subprocess hasn't been assigned yet
                time.sleep(0.1)
                continue
            if self.p.poll() is not None:
                # subprocess was assigned but died
                break
            if self.running == True:
                # subprocess assigned and running correctly
                return True

        # Stop run loop and return False
        self.kill = True
        return False

    # This code is run when the thread starts
    def run(self):
        proc_list = []
        proc_list.append(self.app_path)
        proc_list.append("--device")
        proc_list.append(self.dev_path)
        proc_list.append("--channel")
        proc_list.append(self.channel)
        proc_list.append("--page")
        proc_list.append(self.page)
        proc_list.append("--interface")
        proc_list.append(self.interface)

        # Start sniffing application as subprocess
        self.p = subprocess.Popen(proc_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        # Enable non-blocking reads from standard output of subprocess
        fcntl.fcntl(self.p.stdout.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)
        # Create polling object to poll for I/O events from subprocess file descriptors
        my_poll = select.poll()
        my_poll.register(self.p.stdout.fileno(), select.POLLIN | select.POLLPRI | select.POLLERR)

        while not self.kill:
            # Check subprocess is alive
            if self.p.poll() is not None:
                logging.warning("Sniffer subprocess died")
                self.kill = True
                break

            # Poll subprocess for I/O events
            poll_ret = my_poll.poll(100)
            if len(poll_ret) == 0:
                continue

            # Read output if I/O event was detected
            out = self.p.stdout.read().decode('UTF-8')
            if "application running" in out:
                # Sniffer is running properly
                self.running = True

        self.running = False
        self.close()
        logging.warning("Sniffer thread finished")
        return

    def close(self):
        self.kill = True
        if self.p: self.p.kill()

class ble_sniffer_thread(threading.Thread):
    def __init__ (self, app_path, dev_path, zdd_eui48, interface):
        threading.Thread.__init__(self)
        self.daemon = True
        self.app_path = app_path
        self.dev_path = dev_path
        self.zdd_eui48 = zdd_eui48
        self.interface = interface
        self.p = None
        self.kill = False
        self.running = False

    def check(self):
        return True

    def run(self):
        proc_list = []
        if self.app_path[-3:] == ".py":
            proc_list.append("python3")
        proc_list.append(self.app_path)
        proc_list.append("--extcap-interface")
        proc_list.append(self.dev_path)
        proc_list.append("--tshark-interface")
        proc_list.append(self.interface)
        proc_list.append("--follow-address")
        proc_list.append(self.zdd_eui48)
        self.p = subprocess.Popen(proc_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        self.running = True

        while not self.kill:
            # Check process is alive
            if self.p.poll() is not None:
                logging.warning("BLE Sniffer subprocess died")
                self.kill = True
                break

        self.running = False
        self.close()
        logging.warning("BLE Sniffer process thread finished")
        return

    def close(self):
        self.kill = True
        if self.p: self.p.kill()
