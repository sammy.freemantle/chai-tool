# Copyright [2023] Exegin Technologies Limited.
# This file is part of chai-tool.

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

# Convert GU commands to use old GU r22 syntax
class R22_GU_Utility():
    def __init__ (self):
        self.STORED_REPLACEMENTS = []
        self.AFTER_GU_STARTUP = False


    def reset(self):
        self.STORED_REPLACEMENTS = []
        self.AFTER_GU_STARTUP = False


    def convert_cmd(self, cmd):
        saved_prefix = ""
        ret_cmd = cmd
        ret_list = []

        if "@ignore" in ret_cmd:
            saved_prefix = "@ignore "
            ret_cmd = ret_cmd.split("@ignore ")[1]
        elif "@error" in ret_cmd:
            saved_prefix = "@error "
            ret_cmd = ret_cmd.split("@error ")[1]

        if ret_cmd.startswith("aps_group "):
            ret_cmd = ret_cmd.replace("--group ", "")
            ret_cmd = ret_cmd.replace("--ep ", "")

        elif ret_cmd.startswith("aps_link_key "):
            ret_cmd = ret_cmd.replace("--linkkey", "--key")

        elif ret_cmd.startswith("aps_remove_device "):
            ret_cmd = ret_cmd.replace("--dstext ", "")
            ret_cmd = ret_cmd.replace("--target ", "")

        elif ret_cmd.startswith("aps_switch_key "):
            ret_cmd = ret_cmd.replace("--dstnwk ", "short ")
            ret_cmd = ret_cmd.replace("--seqno ", "")

        elif ret_cmd.startswith("aps_tc_policy "):
            args = [arg.strip("--") for arg in ret_cmd.split(" ")]
            # aps_tc_policy must go after form/join on GU's
            if self.AFTER_GU_STARTUP:
                ret_cmd = " ".join(args)
            else:
                self.STORED_REPLACEMENTS.append(" ".join(args))
                ret_cmd = None

        elif ret_cmd.startswith("aps_update_device "):
            ret_cmd = ret_cmd.replace("--", "")

        elif ret_cmd.startswith("get_ib"):
            if "<" in ret_cmd or ">" in ret_cmd:
                # get_ib with > or < is broken on GU's
                ret_cmd = None

        elif ret_cmd.startswith("msg_filter ") \
            or ret_cmd.startswith("set_ib nwkPanIdConflictThresh ") \
            or ret_cmd.startswith("log_level zcl"):
            ret_cmd = None

        elif ret_cmd.startswith("nwk_disc "):
            ret_cmd = "@ignore startup_join"

        elif ret_cmd.startswith("nwk_end_timeout "):
            ret_cmd = ret_cmd.replace("change ", "")
            ret_cmd = ret_cmd.replace("--extaddr ", "")
            ret_cmd = ret_cmd.replace("--timeout ", "")

        elif ret_cmd.startswith("nwk_joinlist "):
            ret_cmd = ret_cmd.replace("--policy ", "policy ")
            ret_cmd = ret_cmd.replace("--eui_list ", "")

        elif ret_cmd.startswith("nwk_key "):
            ret_cmd = ret_cmd.replace("--key ", "")
            ret_cmd = ret_cmd.replace("--seqno ", "")

        elif ret_cmd.startswith("nwk_poll slow --period "):
            value = ret_cmd.split("nwk_poll slow --period ")[1]
            new_value = str(int(value) * 1000)
            ret_cmd = "set_ib nwkSlowPollPeriod " + new_value

        elif ret_cmd.startswith("nwk_route_request "):
            ret_cmd = ret_cmd.replace("--nwkaddr", "nwk")
            ret_cmd = ret_cmd.replace("--dstext", "ext")

        elif ret_cmd.startswith("nwk_status "):
            nwk_status_cmd = ""
            dstnwk = ""
            nwkaddr = ""
            parameters = ret_cmd.split(" ")
            if parameters[1] == "send-conflict":
                nwk_status_cmd = "conflict"
                for i,param in enumerate(parameters[2:]):
                    if param == "--dstnwk":
                        dstnwk = parameters[i+3]
                    elif param == "--nwkaddr":
                        nwk_addr = parameters[i+3]
                ret_cmd = " ".join(["nwk_status", dstnwk, nwk_addr, nwk_status_cmd])

        elif ret_cmd.startswith("set_ib bdbFlags "):
            ib_cmds = []
            value = int(ret_cmd.split(" ")[-1], 16)
            if value & 1: ib_cmds.append("set_ib bdbJoinIgnoreLqi 1")
            if value & 2: ib_cmds.append("set_ib bdbZdoPermitJoinAfterJoin 0")
            if value & 4: ib_cmds.append("set_ib bdbDisablePersistRejoin 1")
            if value & 16: ib_cmds.append("set_ib bdbNwkAllowRouterLeaveRejoin 1")
            ret_cmd = None
            ret_list.extend(ib_cmds)

        elif ret_cmd.startswith("startup_channellist "):
            args = ret_cmd.split(" ")
            for arg in args:
                if "--" in arg: args.remove(arg)
            ret_cmd = " ".join(args)

        elif ret_cmd.startswith("startup_config "):
            if "networkKey" in ret_cmd:
                key = ""
                nwkseqno = ""
                parameters = ret_cmd.split(" ")
                for i,param in enumerate(parameters):
                    if param == "--networkKey":
                        key = parameters[i+1]
                    if param == "--nwkseqno":
                        nwkseqno = parameters[i+1]
                if self.AFTER_GU_STARTUP:
                    ret_cmd = "nwk_key set " + key + " " + nwkseqno
                else:
                    self.STORED_REPLACEMENTS.append("nwk_key set " + key + " " + nwkseqno)
                    ret_cmd = None
            else:
                ret_cmd = ret_cmd.replace("epid", "extendedpanid")
                ret_cmd = ret_cmd.replace("trustCenterAddress", "security trustCenterAddress")
                ret_cmd = ret_cmd.replace("preconfiguredLinkKey", "security preconfiguredLinkKey")
                args = [arg.strip("--") for arg in ret_cmd.split(" ")]
                ret_cmd = " ".join(args)

        elif ret_cmd.startswith("startup_form") or ret_cmd.startswith("startup_join"):
            ret_cmd = ret_cmd.replace("--attempts ", "")
            self.AFTER_GU_STARTUP = True
            if len(self.STORED_REPLACEMENTS) > 0:
                group = len(self.STORED_REPLACEMENTS) + 1
                mark_cmd = "#tshark-mark: group=" + str(group)
                cmds = [mark_cmd, saved_prefix + ret_cmd]
                for replacement in self.STORED_REPLACEMENTS:
                    cmds.append(saved_prefix + replacement)
                self.STORED_REPLACEMENTS = []
                ret_cmd = None
                ret_list.extend(cmds)

        elif ret_cmd.startswith("startup_persist "):
            ret_cmd = ret_cmd.replace("--file ", "")

        elif ret_cmd.startswith("zcl_tp2_buffer "):
            ret_cmd = ret_cmd.replace("--rreq ", "")

        elif ret_cmd.startswith("zdo_bind_req ") or ret_cmd.startswith("zdo_unbind_req "):
            ret_cmd = ret_cmd.replace("--srcext ", "")
            ret_cmd = ret_cmd.replace("--srcep ", "")
            ret_cmd = ret_cmd.replace("--dstext ", "ext ")
            ret_cmd = ret_cmd.replace("--dstep ", "")
            ret_cmd = ret_cmd.replace("--cluster ", "")
            ret_cmd = ret_cmd.replace("--group ", "group ")

        elif ret_cmd.startswith("zdo_device_annce "):
            ret_cmd = ret_cmd.replace("--nwkaddr", "--nwk")
            ret_cmd = ret_cmd.replace("--extaddr", "--ext")
            ret_cmd = ret_cmd.replace("send ", "")

        elif ret_cmd.startswith("zdo_match_desc_req "):
            ret_cmd = ret_cmd.replace(",", " ")
            ret_cmd = ret_cmd.replace("--", "")
            ret_cmd = ret_cmd.replace("dstnwk ", "")
            ret_cmd = ret_cmd.replace("nwkaddr ", "")
            ret_cmd = ret_cmd.replace("profile ", "")

        elif ret_cmd.startswith("zdo_mgmt_bind_req "):
            ret_cmd = ret_cmd.replace("--block", "")

        elif ret_cmd.startswith("zdo_mgmt_lqi_req "):
            ret_cmd = ret_cmd.replace("--nwkaddr ", "")
            ret_cmd = ret_cmd.replace("--index ", "")

        elif ret_cmd.startswith("zdo_mgmt_nwk_joining_list "):
            ret_cmd = ret_cmd.replace("--index ", "")

        elif ret_cmd.startswith("zdo_mgmt_nwk_update_req "):
            args = ret_cmd.split(" ")
            for arg in args:
                if "--" in arg: args.remove(arg)
            ret_cmd = " ".join(args)

        elif ret_cmd.startswith("zdo_nwk_addr_req "):
            ret_cmd = ret_cmd.replace("--dstnwk", "--dest")

        elif ret_cmd.startswith("zdo_raw_req "):
            cmd = ""
            nwkaddr = ret_cmd.split(" ")[1]
            cluster_id = ret_cmd.split(" ")[2]
            payload = ret_cmd.split(" ")[3]
            if cluster_id == "0x0015":
                cmd = "zdo_system_server_disc_req"
                payload = payload[2:4] + payload[0:2]
                ret_cmd = (" ").join([cmd, "0x" + payload])

        if ret_cmd is None:
            return ret_list

        return [saved_prefix + ret_cmd]